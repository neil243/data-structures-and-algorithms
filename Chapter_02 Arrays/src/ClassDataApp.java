//******************************************************************
// Author: Neil Kasanda
//
// Program: a class to test the various functionalities of the
// ClassDataArray
//******************************************************************

public class ClassDataApp 
{
	public static void main(String[] args)
	{
		int maxSize = 100;									// array size
		ClassDataArray arr;									// reference to array
		arr = new ClassDataArray(maxSize);					// create the array
		
			// insert 10 items
		arr.insert("Patty", "Evans", 24);
		arr.insert("Lorraine", "Smith", 37);
		arr.insert("Tom", "Yee", 43);
		arr.insert("Henry", "Adams", 63);
		arr.insert("Sato", "Hashimoto", 21);
		arr.insert("Henry", "Stimson", 29);
		arr.insert("Jose", "Velasquez", 72);
		arr.insert("Henry", "Lamarque", 54);
		arr.insert("Minh", "Vang", 22);
		arr.insert("Lucinda", "Creswell", 18);
		
			// display items
		arr.displayA();
		System.out.println();
		
			// search for item
		String searchKey = "Stimson";
		Person found;
		found = arr.find(searchKey);
		
		if(found != null)
		{
			System.out.print("Found ");
			found.displayPerson();
		}
		else
			System.out.println("Can't find " + searchKey);
		System.out.println();
		
		System.out.println("Deleting Smith, Yee, and Creswell");
		arr.delete("Smith");
		arr.delete("Yee");
		arr.delete("Creswell");
		
			// display items again
		arr.displayA();
	}
}
