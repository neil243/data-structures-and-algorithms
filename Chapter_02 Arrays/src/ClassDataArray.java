//********************************************************************
// Author: Neil Kasanda
//
// Program:
// A class that uses objects in the data storage structure instead
// of the primitive type "long"
//********************************************************************

public class ClassDataArray 
{
	private Person[] a;							// reference to array
	private int nElems;							// number of data items
	
		// constructor
	public ClassDataArray(int size)
	{
		a = new Person[size];					// create/instantiate array
		nElems = 0;								// no items yet
	}
	
	public Person find(String searchName)		// find specified value
	{
		int j;
		
		for(j = 0; j < nElems; j++)					// for each element,
			if(a[j].getLast().equals(searchName))	// found item?
				break;								// exit loop early
		
		if(j == nElems)								// gone to end?
			return null;							// yes, can't find it
		else
			return a[j];							// no, found it
	}// end find()
	
		//put person into array
	public void insert(String firstName, String lastName, int age)
	{
		a[nElems] = new Person(firstName, lastName, age);
		nElems++;											// increment size
	}
	
		// delete person from array
	public boolean delete(String searchName)
	{
		int j;
		
		for(j = 0; j < nElems; j++)							// look for key
			if(a[j].getLast().equals(searchName))
				break;
		
		if(j == nElems)										// can't find it
			return false;
		
		else												// found it
		{
			for(int k = j; k < nElems - 1; k++)				// shift down
				a[k] = a[k + 1];
			
			nElems--;										// decrement size
			return true;
		}
	}//end delete
	
	public void displayA()									// displays array contents
	{
		for(int j = 0; j < nElems; j++)
			a[j].displayPerson();
	}
}// end ClassDataArray