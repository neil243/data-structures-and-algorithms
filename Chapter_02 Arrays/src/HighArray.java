//****************************************************************************
// Author: Neil Kasanda
//
// Program: demonstrates array class with high-level interface
// Listing 2.3
//****************************************************************************
public class HighArray 
{
	private long[] a;
	private int nElems;
	
	public HighArray(int size)
	{
		a = new long[size];
		nElems = 0;
	}
	
	public boolean find(long searchKey)
	{
		int j;
		
		for(j = 0; j < nElems; j++)			// for each element,
			if(a[j] == searchKey)			// found item?
				break;
		
		if(j == nElems)						// not found
			return false;
		else
			return true;					// found
	}// end find
	
	public void insert(long value)			// put element into array
	{
		a[nElems] = value;					// insert it
		nElems++;							// increment size
	}
	
	public boolean delete(long value)
	{
		int j;
		
		for(j = 0; j < nElems; j++)			// look for it
			if(a[j] == value)
				break;
		
		if(j == nElems)						// Can't find it
			return false;
		else								// found it
		{
			for(int k = j; k < nElems - 1; k++)
				a[k] = a[k + 1];
			nElems--;
			return true;
		}
	}//end delete
	
	public void display()					// display array contents
	{
		for(int j = 0; j < nElems; j++)		// for each element,
			System.out.print(a[j] + " ");	// display it
		System.out.println();
	}
}// end class HighArray
