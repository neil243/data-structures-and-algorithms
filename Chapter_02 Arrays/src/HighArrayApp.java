//****************************************************************************
// Author: Neil Kasanda
//
// Program: to test the various functionalities of the HighArray class
// Listing 2.3 (continued)
//****************************************************************************
public class HighArrayApp 
{
	public static void main(String[] args)
	{
		int maxSize = 100;							// array size
		HighArray arr = new HighArray(maxSize);		// create the array
		
			// insert 10 items
		arr.insert(77);
		arr.insert(99);
		arr.insert(44);
		arr.insert(55);
		arr.insert(22);
		arr.insert(88);
		arr.insert(11);
		arr.insert(00);
		arr.insert(63);
		arr.insert(33);
		
		arr.display();								// display items
		
			// search for item
		int searchKey = 35;
		if(arr.find(searchKey))
			System.out.println("Found " + searchKey);
		else
			System.out.println("Can't find " + searchKey);
		
			// delete 3 items
		arr.delete(00);
		arr.delete(55);
		arr.delete(99);
		
			// display items again
		arr.display();
	}//end main
}//end HighArrayApp
