//*************************************************************************
// Author: Neil Kasanda
//
// Program: demonstrates array class with low-level interface
// Listing 2.2
//*************************************************************************

public class LowArray 
{
		// instance variables
	private long[] a;
	
		// constructor
	public LowArray(int size)
	{
		a = new long[size];					// instantiate array
	}
	
		// set value
	public void setElem(int index, long value)
	{
		a[index] = value;
	}
	
		// get value
	public long getElem(int index)
	{
		return a[index];
	}
} // end class LowArray
