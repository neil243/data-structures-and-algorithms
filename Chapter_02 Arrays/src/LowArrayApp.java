//*****************************************************************************
// Author: Neil Kasanda
//
// Program: to test the various functionalities of the LowArray class
// Listing 2.2 (continued)
//*****************************************************************************

public class LowArrayApp 
{
	public static void main(String[] args)
	{
		LowArray arr = 
				new LowArray(100);	// create and instantiate LowArray object
		int nElems = 0;				// number of items in array
		int j;						// loop variable
		
			// insert 10 items
		arr.setElem(0, 77);
		arr.setElem(1, 99);
		arr.setElem(2, 44);
		arr.setElem(3, 55);
		arr.setElem(4, 22);
		arr.setElem(5, 88);
		arr.setElem(6, 11);
		arr.setElem(7, 00);
		arr.setElem(8, 66);
		arr.setElem(9, 33);
		nElems = 10;				// now 10 items are in array
		
			// display items
		for(j = 0; j < nElems; j++)
			System.out.print(arr.getElem(j) + " ");
		System.out.println();
		
			// search for data item
		int searchKey = 26;
		for(j = 0; j < nElems; j++)			// for each element,
			if(arr.getElem(j) == searchKey)	// found item?
				break;
		
		if(j == nElems)						// no, did not find it
			System.out.println("Can't find " + searchKey);
		else								// yes, found it
			System.out.println("Found " + searchKey);
		
			// delete value 55
		searchKey = 55;
		for(j = 0; j < nElems; j++)			// for each element,
			if(arr.getElem(j) == searchKey)	// found item?
				break;
		
		for(int k = j; k < nElems - 1; k++)	// higher ones down
			arr.setElem(k, arr.getElem(k + 1));
		nElems--;							// decrement size
		
			// display items
		for(j = 0; j < nElems; j++)
			System.out.print(arr.getElem(j) + " ");
		System.out.println();
	
	}// end main
}// end class LowArrayApp
