//*************************************************************
// Author: Neil Kasanda
//
// Program: demonstrates ordered array class
// Listing 2.4
//*************************************************************

public class OrderedArray 
{
		// instance variables
	private long[] a;
	private int nElems;
	
		// constructor
	public OrderedArray(int size)
	{
		a = new long[size];
		nElems = 0;
	}
	
	public int size()
	{
		return nElems;
	}
	
	public int find(long searchKey)
	{
		int lowerBound = 0;
		int upperBound = nElems - 1;
		int curIn;
		
		while(true)
		{
			curIn = (lowerBound + upperBound) / 2;
			
			if(a[curIn] == searchKey)
				return curIn;						// found it
			else if(lowerBound > upperBound)
				return nElems;						// can't find it
			
			else									// divide
			{
				if(a[curIn] < searchKey)
					lowerBound = curIn + 1;			// it's in upper half
				else
					upperBound = curIn - 1;			// it's in lower half
			}// end else divide range
		}// end while
	}// end find()
	
	public void insert(long value)
	{
		int j;
		
		for(j = 0; j < nElems; j++)					// find where it goes
			if(a[j] > value)						// (linear search)
				break;
		
		for(int k = nElems; k > j; k--)				// move bigger ones up
			a[k] = a[k - 1];
		
		a[j] = value;								// insert it
		nElems++;									// increment size
	}
	
	public boolean delete(long value)
	{
		int j = find(value);
		
		if(j == nElems)
			return false;
		else
		{
			for(int k = j; k < nElems - 1; k++)
				a[k] = a[k + 1];
			
			nElems--;
			return true;
		}
	}// end delete
	
	public void display()
	{
		for(int j = 0; j < nElems; j++)				// for each element
			System.out.print(a[j] + " ");			// display it
		System.out.println();
	}
}
