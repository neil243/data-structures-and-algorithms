//************************************************************************
// Author: Neil Kasanda
//
// Program: to test the various functionalities of the OrderedArray class
// Listing 2.4 (continued)
//************************************************************************

public class OrderedArrayApp 
{
	public static void main(String[] args)
	{
		int maxSize = 100;								// array size
		OrderedArray arr;								// reference to array
		arr = new OrderedArray(maxSize);				// create the array
		
			// insert 10 items
		arr.insert(77);
		arr.insert(99);
		arr.insert(44);
		arr.insert(55);
		arr.insert(22);
		arr.insert(88);
		arr.insert(11);
		arr.insert(00);
		arr.insert(66);
		arr.insert(33);
		
		arr.display();
		
			// search for item
		int searchKey = 65;
		
		if(arr.find(searchKey) != arr.size())
			System.out.println("Found " + searchKey);
		else
			System.out.println("Can't find " + searchKey);
		
			// display items
		arr.display();
		
			// delete 3 items
		arr.delete(00);
		arr.delete(55);
		arr.delete(99);
		
			// display items again
		arr.display();
	}// end main
}//end OrderedArrayApp
