//****************************************************************
// Author: Neil Kasanda
//
// Program: demonstrates bubble sort.
//****************************************************************

public class ArrayBub 
{
	private long[] a;									// ref to array a
	private int nElems;									// number of data items
	
		// constructor
	public ArrayBub(int size)
	{
		a = new long[size];
		nElems = 0;
	}
	
		// put elements into array
	public void insert(long value)
	{
		a[nElems] = value;								// insert it
		nElems++;										// increment size
	}
	
		// display array contents
	public void display()
	{
		for(int j = 0; j < nElems; j++)					// for each element,
			System.out.print(a[j] + " ");				// display it
		System.out.println();
	}
	
	public void bubbleSort()
	{
		int out, in;
		
		for(out = nElems - 1; out > 0; out--)
			for(in = 0; in < out; in++)
				if(a[in] > a[in + 1])
					swap(in, in + 1);
	}//end bubbleSort
	
	public void swap(int one, int two)
	{
		long temp = a[one];
		a[one] = a[two];
		a[two] = temp;
	}
}// end class ArrayBub