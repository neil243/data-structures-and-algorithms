//*********************************************************
// Author: Neil Kasanda
//
// Program: class that uses an array storage structure to
// store objects of type Person
//*********************************************************
public class ArrayInOb 
{
		// instance variables
	private Person[] a;
	private int nElems;
	
		// constructor
	public ArrayInOb(int size)
	{
		a = new Person[size];
		nElems = 0;
	}
	
	public void insert(String first, String last, int age)
	{
		a[nElems] = new Person(first, last, age);
		nElems++;
	}
	
	public void display()
	{
		for(int j = 0; j < nElems; j++)
			a[j].displayPerson();
		System.out.println();
	}
	
	public void insertionSort()
	{
		int out, in;
		Person temp;
		
		for(out  = 1; out < nElems; out++)
		{
			temp = a[out];
			in = out;
			
			while(in > 0 && (a[in - 1].getLast().compareTo(temp.getLast()) > 0))
			{
				a[in] = a[in - 1];
				in--;
			}
			a[in] = temp;
		}//end for
	}//end insertionSort()
}//end class ArrayInOb
