//*******************************************************
// Author: Neil Kasanda
//
// Program: demonstrates insertion sort
//*******************************************************

public class ArrayIns 
{
		// instance variables
	private long[] a;
	private int nElems;
	
		// constructor
	public ArrayIns(int size)
	{
		a = new long[size];
		nElems = 0;
	}
	
		// put element into array
	public void insert(long value)
	{
		a[nElems] = value;
		nElems++;
	}
	
		// display array contents
	public void display()
	{
		for(int j = 0; j < nElems; j++)					// for each element,
			System.out.print(a[j] + " ");				// display it
		System.out.println();
	}
	
	public void insertionSort()
	{
		int out, in;
		long temp;
		
		for(out = 1; out < nElems; out++)
		{
			temp = a[out];
			in = out;
			
			while(in > 0 && a[in - 1] >= temp)
			{
				a[in] = a[in - 1];
				in--;
			}
			
			a[in] = temp;
		}//end for
	}// end insertionSort
}//end class ArrayIns