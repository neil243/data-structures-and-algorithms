
public class InsertSortApp 
{
	public static void main(String[] args)
	{
		int maxSize = 100;								// array size
		ArrayIns arr = new ArrayIns(maxSize);			// create array
		
			// insert 10 items
		arr.insert(77);
		arr.insert(99);
		arr.insert(44);
		arr.insert(55);
		arr.insert(22);
		arr.insert(88);
		arr.insert(11);
		arr.insert(00);
		arr.insert(66);
		arr.insert(33);
		
			// display items
		arr.display();
		
			// insertion-sort them
		arr.insertionSort();
		
			// display them again
		arr.display();
	}//end main
}//end class InsertSortApp
