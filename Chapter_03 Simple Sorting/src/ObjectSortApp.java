//*********************************************************
// Author: Neil Kasanda
//
// Program: to test the various operations of ArrayInOb,
// which uses insertion sort to sort an array of objects
//*********************************************************
public class ObjectSortApp 
{
	public static void main(String[] args)
	{
		int maxSize = 100;										// array size
		ArrayInOb arr = new ArrayInOb(maxSize);					// create array
		
			// insert 10 Person items
		arr.insert("Patty", "Evans", 24);
		arr.insert("Doc", "Smith", 59);
		arr.insert("Lorraine", "Smith", 37);
		arr.insert("Paul", "Smith", 37);
		arr.insert("Tom", "Yee", 43);
		arr.insert("Sato", "Hashimoto", 21);
		arr.insert("Henry", "Stimson", 29);
		arr.insert("Jose", "Velasquez", 72);
		arr.insert("Minh", "Vang", 22);
		arr.insert("Lucinda", "Creswell", 18);
		
			// display items before insertionSort
		System.out.println("Before sorting:");
		arr.display();
		
			// insertion-sort items
		arr.insertionSort();
		
			// display items after insertionSort
		System.out.println("After Sorting:");
		arr.display();
	}// end main
}// end ObjectSortArray
