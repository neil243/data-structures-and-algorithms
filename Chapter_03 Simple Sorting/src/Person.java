//*********************************************************
// Author: Neil Kasanda
//
// Program: a class that implements some basic properties
// of a person
//*********************************************************
public class Person 
{
		// instance variables
	private String firstName;
	private String lastName;
	private int age;
	
		// constructor
	public Person(String first, String last, int a)
	{
		firstName = first;
		lastName = last;
		age = a;
	}
	
	public void displayPerson()
	{
		System.out.print("   Last name: " + lastName);
		System.out.print(", First name: " + firstName);
		System.out.println(", Age: " + age);
	}
	
	public String getLast()
	{
		return lastName;
	}
}//end class Person
