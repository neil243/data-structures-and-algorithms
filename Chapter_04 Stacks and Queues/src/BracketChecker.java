//*************************************************
// Author: Neil Kasanda
//
// Program: to check matching delimiters
//*************************************************

public class BracketChecker 
{
		// instance variables
	private String input;
	
		// constructor
	public BracketChecker(String in)
	{
		input = in;
	}
	
	public void check()
	{
		int stackSize = input.length();					// get max stack size
		StackZ theStack = new StackZ(stackSize);		// make stack
		
		for(int j = 0; j < stackSize; j++)
		{
			char ch = input.charAt(j);
			
			switch(ch)
			{
				// opening symbols
			case '{':
			case '[':
			case '(':
				theStack.push(ch);								// push them
				break;
				
				// closing symbols
			case '}':
			case ']':
			case ')':
				if(!theStack.isEmpty())							// stack is not empty
				{
					char chx = theStack.pop();
					if( (chx == '{' && chx != '}') || 
							(chx == '[' && chx != ']') || 
							(chx == '(' && chx != ')'))
						System.out.println("Error: " + ch + " at " + j);
				}
				else											// prematurely empty
					System.out.println("Error: " + ch + " at " + j);
				break;
				
			default:
				break;		// no action on other characters
			}//end switch
		}//end for
		
			// at this point, all the characters have been processed
		if(!theStack.isEmpty())
			System.out.println("Error: missing right delimiter");
	}//end check
}//end class BracketChecker