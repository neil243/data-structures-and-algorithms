//***************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class
// BracketChecker
//***************************************************

import java.io.*;

public class BracketsApp 
{
	public static void main(String[] args) throws IOException
	{
		String input;
		
		while(true)
		{
			System.out.print("Enter a string containing delimiters: ");
			input = getString();
			System.out.println();
			
			if(input.equals(""))		// quit if [Enter]
				break;
			
				// make a BracketChecker
			BracketChecker theChecker = new BracketChecker(input);
			theChecker.check();			// check brackets
		}//end while
	}//end main
	
	public static String getString() throws IOException
	{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		return s;
	}
} //end class BracketsApp