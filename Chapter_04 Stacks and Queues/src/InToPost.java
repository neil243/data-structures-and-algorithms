//***********************************************************
// Author: Neil Kasanda
//
// Program: converts infix arithmetic expressions to postfix
//***********************************************************

public class InToPost 
{
		// instance variables
	private StackInPos theStack;
	private String input;
	private String output;
	
		// constructor
	public InToPost(String in)
	{
		input = in;
		theStack = new StackInPos(input.length());
		output = "";
	}
	
		// do translation to post-fix
	public String doTrans()
	{
		char ch;
		
		for(int j = 0; j < input.length(); j++)
		{
			ch = input.charAt(j);
			theStack.displayStack("For " + ch + " ");	// *diagnostic*
			
			switch(ch)
			{
			case '+':
			case '-':
				gotOper(ch, 1);							// go pop operators
				break;									// + and - have precedence 1
				
			case '*':
			case '/':
				gotOper(ch, 2);							// go pop operators
				break;									// * and / have precedence 2
				
			case '(':									// it's a left parenthesis
				theStack.push(ch);						// push it onto stack
				break;
				
			case ')':									// it's a right parenthesis
				gotParen(ch);							// go pop operators
				break;
				
			default:									// must be an operand
				output = output + ch;					// write it to output
			}//end switch
		
		}//end for
		
		while(!theStack.isEmpty())						// pop remaining operators
		{
			theStack.displayStack("While "); 			// *diagnostic*
			output = output + theStack.pop();			// append to output string
		}
		
		theStack.displayStack("End ");					// *diagnostic*
		return output;									// return postfix
	}//end doTrans()
	
		// got operator from input
	public void gotOper(char opThis, int prec1)
	{
		char opTop;
		
		while(!theStack.isEmpty())
		{
			opTop = theStack.pop();
			
			if(opTop == '(')
			{
				theStack.push(opTop);				// put it back
				break;
			}
			else									// it's an operator
			{
					// precedence of popped op
				int prec2;
				
				if(opTop == '+' || opTop =='-')		// find precedence of
					prec2 = 1;						// popped operator
				else
					prec2 = 2;
				
				if(prec2 < prec1)					// if prec of new op less
				{									//		than prec of old
					theStack.push(opTop);			// save newly-popped op
					break;
				}
				else								// prec of new not less
					output = output + opTop;		//		than prec of old
			}//end else (it's an operator)
		}//end while
		theStack.push(opThis);						// push new operator
	}//end gotOp()
	
		// got right parenthesis from input
	public void gotParen(char ch)
	{
		char chx;
		while(!theStack.isEmpty())
		{
			chx = theStack.pop();
			
			if(chx == '(')				// if popped '('
				break;					// we're done
			
			else						// if popped operator
				output = output + chx;	// output it
		}//end while
	}//end gotParen()
}//end class InToPost