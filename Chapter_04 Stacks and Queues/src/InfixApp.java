//***********************************************************
// Author: Neil Kasanda
//
// Program: to test the various operations of the class
// InToPost
//***********************************************************

import java.io.*;

public class InfixApp 
{
	public static void main(String args[]) throws IOException
	{
		String input, output;
		
		while(true)
		{
			System.out.print("Enter infix: ");
			input = getString();
			System.out.println();
			
			if(input.equals(""))
				break;
			
				// make a translator
			InToPost theTrans = new InToPost(input);
			output = theTrans.doTrans();
			System.out.println("Postfix is " + output + "\n");
		}//end while
	}//end main
	
	public static String getString() throws IOException
	{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		return s;
	}
}//end class InfixApp