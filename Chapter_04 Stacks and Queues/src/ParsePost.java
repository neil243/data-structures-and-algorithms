//***********************************************************
// Author: Neil Kasanda
//
// Program: to parse a postfix expression and evaluate it
//***********************************************************

public class ParsePost 
{
	private StackPostfixEval theStack;
	private String input;
	
	public ParsePost(String in)
	{
		input = in;
	}
	
	public int doParse()
	{
		theStack = new StackPostfixEval(input.length());
		char ch;
		int j;
		int num1, num2, interAns;
		
		interAns = 0;
		
		for(j = 0; j < input.length(); j++)
		{
			ch = input.charAt(j);
			theStack.displayStack(ch + " ");
			
			if(ch >= '0' && ch <= '9')
				theStack.push(ch - '0');
			else
			{
				num2 = theStack.pop();
				num1 = theStack.pop();
				
				switch(ch)
				{
				case '+':
					interAns = num1 + num2;
					break;
					
				case '-':
					interAns = num1 - num2;
					break;
					
				case '*':
					interAns = num1 * num2;
					break;
					
				case '/':
					interAns = num1 / num2;
					break;
				}
				theStack.push(interAns);					// push result
			}//end else
		}//end for		
		
		return interAns;
	}//end doParse
}// end ParsePost