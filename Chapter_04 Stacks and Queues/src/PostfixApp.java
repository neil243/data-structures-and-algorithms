//***********************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class ParsePost
//***********************************************************

import java.io.*;

public class PostfixApp 
{
	public static void main(String[] args) throws IOException
	{
		String input;
		int output;
		
		while(true)
		{
			System.out.print("Enter postfix: ");
			input = getString();
			System.out.println();
			
			if(input.equals(""))
				break;
			
			ParsePost aParser = new ParsePost(input);
			output = aParser.doParse();
			System.out.println("Evaluates to " + output);
		}//end while
	}//end main()
	
	public static String getString() throws IOException
	{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		
		return br.readLine();
	}
}//end class PostfixApp