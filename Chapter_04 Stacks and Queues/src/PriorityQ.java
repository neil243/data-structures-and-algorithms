//******************************************************
// Author: Neil Kasanda
//
// Program: demonstrates a priority queue
//******************************************************

public class PriorityQ 
{
		// array in sorted order, from max at 0 to min at size-1 
		// instance variables
	private int maxSize;
	private long[] queArray;
	private int nItems;
	
		// constructor
	public PriorityQ(int size)
	{
		maxSize = size;
		queArray = new long[maxSize];
		nItems = 0;
	}
	
	public void insert(long item)
	{
		int j;
		
		for(j = nItems - 1; j >= 0; j--)
		{
			if(item > queArray[j])
				queArray[j + 1] = queArray[j];
			else
				break;
		}// end for
		
		queArray[j + 1] = item;
		nItems++;
	}//end insert
	
		// remove minimum item
	public long remove()
	{
		return queArray[nItems-- - 1];
	}
	
		// peek at minimum item
	public long peekMin()
	{
		return queArray[nItems - 1];
	}
	
		// true if queue is empty
	public boolean isEmpty()
	{
		return nItems == 0;
	}
	
		// true if queue is full
	public boolean isFull()
	{
		return nItems == maxSize;
	}
}//end class PriorityQ