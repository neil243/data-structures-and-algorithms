//******************************************************
// Author: Neil Kasanda
//
// Program: demonstrates a priority queue
//******************************************************

public class PriorityQApp 
{
	public static void main(String[] args)
	{
		int maxSize = 5;					// priority queue size
		PriorityQ thePQ = new PriorityQ(maxSize);
		
			// insert 5 items
		thePQ.insert(30);
		thePQ.insert(50);
		thePQ.insert(10);
		thePQ.insert(40);
		thePQ.insert(20);
		
		while(!thePQ.isEmpty())
		{
			long item = thePQ.remove();
			System.out.print(item + " ");
		}
		System.out.println();
	}//end main
}//end class PriorityQApp
