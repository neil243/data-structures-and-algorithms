//************************************************
// Author: Neil Kasanda
//
// Program: demonstrates a queue
//************************************************

public class Queue 
{
		// instance variables
	private int maxSize;
	private long[] queArray;
	private int front;
	private int rear;
	private int nItems;
	
		// constructor
	public Queue(int size)
	{
		maxSize = size;
		queArray = new long[maxSize];
		front = 0;
		rear = -1;
		nItems = 0;
	}
	
		// put item at rear of queue
	public void insert(long j)
	{
		rear = (rear + 1) % queArray.length;				// deals with wraparound
		queArray[rear] = j;
		nItems++;
	}
	
		// take item from front of queue
	public long remove()
	{
		long temp = queArray[front];
		front = (front + 1) % queArray.length;				// deals with wraparound
		nItems--;
		
		return temp;
	}
	
		// peek at front of queue
	public long peekFront()
	{
		return queArray[front];
	}
	
		// true if queue is empty
	public boolean isEmpty()
	{
		return nItems == 0;
	}
	
		// true if queue is full
	public boolean isFull()
	{
		return nItems == maxSize;
	}
	
		// number of items in queue
	public int size()
	{
		return nItems;
	}
}// end class Queue