//****************************************************
// Author: Neil Kasanda
//
// Program: to test various operations of Queue class
//****************************************************

public class QueueApp 
{
	public static void main(String[] args)
	{
		int maxSize = 5;						// size of queue
		Queue theQueue = new Queue(maxSize);	// create the queue
		
			// insert 4 items
		theQueue.insert(10);
		theQueue.insert(20);
		theQueue.insert(30);
		theQueue.insert(40);
		
			// remove 3 items: 10, 20 and 30
		theQueue.remove();
		theQueue.remove();
		theQueue.remove();
		
			// insert 4 more items
		theQueue.insert(50);
		theQueue.insert(60);
		theQueue.insert(70);
		theQueue.insert(80);
		
		while(!theQueue.isEmpty())
		{
			long n = theQueue.remove();
			System.out.print(n + " ");
		}
		System.out.println();
	}//end main
}//end QueueApp