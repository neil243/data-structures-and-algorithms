//*****************************************
// Author: Neil Kasanda
//
// Program: The Queue class without nItems
//*****************************************

public class QueueWoCount 
{
		// instance variables
	private int maxSize;
	private long[] queArray;
	private int front;
	private int rear;
	
		// constructor
	public QueueWoCount(int size)
	{
		maxSize = size + 1;					// array is 1 cell larger
		queArray = new long[maxSize];		// than requested
		front = 0;
		rear = -1;
	}
	
		// put item at rear of queue
	public void insert(long j)
	{
		rear = (rear + 1) % maxSize;
		queArray[rear] = j;
	}
	
	
	public long remove()
	{
		long temp = queArray[front];
		front = (front + 1) % maxSize;
		
		return temp;
	}
	
	public long peek()
	{
		return queArray[front];
	}
	
		// true if queue is empty
	public boolean isEmpty()
	{
		return ((rear + 1 == front) || (front + maxSize - 1 == rear));
	}
	
		// true if queue is full
	public boolean isFull()
	{
		return ( (rear + 2 == front) || (front + maxSize - 2 == rear));
	}
	
		// assumes queue not empty
	public int size()
	{
		if(rear >= front)			// contiguous sequence
			return rear - front + 1;
		else						// broken sequence
			return (maxSize - front) + (rear + 1);
	}
}// end class Queue