//****************************************************
// Author: Neil Kasanda
//
// Program: to test the various operations of the class
// Reverser
//****************************************************

import java.io.*;

public class ReverseApp 
{
	public static void main(String[] args) throws IOException
	{
		String input;
		String output;
		
		while(true)
		{
			System.out.print("Enter a string: ");			// read a string from kbd
			input = getString();							// quit if [Enter]
			if(input.equals(""))
				break;
			
				// make a Reverser
			Reverser theReverser = new Reverser(input);
			output = theReverser.doRev();					// get the reverse of input
			System.out.println("Reversed: " + output);
		}//end while
	}// end main()
	
	public static String getString() throws IOException
	{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		
		return s;
	}
}