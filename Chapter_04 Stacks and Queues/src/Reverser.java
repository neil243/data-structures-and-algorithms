//**************************************************
// Author: Neil Kasanda
//
// Program: uses a stack to reverse a string
//**************************************************

public class Reverser 
{
	private String input;							// input string
	private String output;							// output string
	
		// constructor
	public Reverser(String in)
	{
		input = in;
	}
	
		// reverse the string
	public String doRev()
	{
		int stackSize = input.length();				// get max stack size
		StackY theStack = new StackY(stackSize);	// make stack
		char ch;
		
		for(int j = 0; j < stackSize; j++)
		{
			ch = input.charAt(j);					// get a char from input
			theStack.push(ch);						// push it
		}
		
		output = "";
		
		while(!theStack.isEmpty())
		{
			ch = theStack.pop();
			output = output + ch;
		}
		
		return output;
	}//end doRev()
}