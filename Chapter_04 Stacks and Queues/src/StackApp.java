//*****************************************************
// Author: Neil Kasanda
//
// Program: to test the various operations of the
// class StackX.
//*****************************************************

public class StackApp 
{
	public static void main(String[] args)
	{
		StackX theStack = new StackX(10);	// make new stack
		
			// push items onto stack
		theStack.push(20);
		theStack.push(40);
		theStack.push(60);
		theStack.push(80);
		
		long value;
		
		while(!theStack.isEmpty())
		{
			value = theStack.pop();
			System.out.print(value + " ");		// display it
		}//end while
		System.out.println();
	}//end main()
}//end class StackApp