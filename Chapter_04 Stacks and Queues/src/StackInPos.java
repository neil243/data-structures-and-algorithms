//***********************************************************
// Author: Neil Kasanda
//
// Program: helper class implementing a stack of characters. 
// helps converts infix arithmetic expressions to postfix
//***********************************************************

public class StackInPos 
{
		// instance variables
	private int maxSize;
	private char[] stackArray;
	private int top;
	
		// constructor
	public StackInPos(int size)
	{
		maxSize = size;
		stackArray = new char[maxSize];
		top = -1;
	}
	
		// push item on top of stack
	public void push(char j)
	{
		top++;
		stackArray[top] = j;
	}
	
		// take item from top of stack
	public char pop()
	{
		return stackArray[top--];
	}
	
		// peek at top of stack
	public char peek()
	{
		return stackArray[top];
	}
	
		// true if stack is empty
	public boolean isEmpty()
	{
		return top == -1;
	}
	
		// true if stack is full
	public boolean isFull()
	{
		return top == maxSize - 1;
	}
	
	public int size()
	{
		return top + 1;
	}
	
		// return item at index n
	public char peekN(int n)
	{
		return stackArray[n];
	}
	
	public void displayStack(String s)
	{
		System.out.print(s);
		System.out.print("Stack (bottom-->top): ");
		for(int j = 0; j < size(); j++)
		{
			System.out.print(peekN(j) + " ");
		}
		System.out.println();
	}
}//end class StackInPos