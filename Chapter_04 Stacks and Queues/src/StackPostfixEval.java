//************************************************************************
// Author: Neil Kasanda
//
// Program: to implement a stack of integers for use in ParsePost class
//************************************************************************

public class StackPostfixEval 
{
		// instance variables
	private int maxSize;
	private int[] stackArray;
	private int top;
	
		// constructor
	public StackPostfixEval(int size)
	{
		maxSize = size;
		stackArray = new int[maxSize];
		top = -1;
	}
	
		// push item on top of stack
	public void push(int j)
	{
		top++;
		stackArray[top] = j;
	}
	
		// take item from top of stack
	public int pop()
	{
		return stackArray[top--];
	}
	
		// peek at top of stack
	public int peek()
	{
		return stackArray[top];
	}
	
		// true if stack is empty
	public boolean isEmpty()
	{
		return top == -1;
	}
	
		// true if stack is full
	public boolean isFull()
	{
		return top == maxSize - 1;
	}
	
		// return size
	public int size()
	{
		return top + 1;
	}
	
		// peek at index n
	public int peekN(int n)
	{
		return stackArray[n];
	}
	
		// display stack
	public void displayStack(String s)
	{
		System.out.print(s);
		System.out.print("Stack (bottom-->top): ");
		
		for(int j = 0; j < size(); j++)
			System.out.print(peekN(j) + " ");
		
		System.out.println();
	}
}// end class StackPostfixEval