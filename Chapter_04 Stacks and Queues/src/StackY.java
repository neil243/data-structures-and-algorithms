//**************************************************
// Author: Neil Kasanda
//
// Program: implements a stack of characters
//**************************************************

public class StackY
{
		// instance variables
	private int maxSize;
	private char[] stackArray;
	private int top;
	
		// constructor
	public StackY(int size)
	{
		maxSize = size;
		stackArray = new char[maxSize];
		top = -1;
	}
	
		// put item on top of stack
	public void push(char j)
	{
		top++;
		stackArray[top] = j;
	}
	
		// take item from top of stack
	public char pop()
	{
		return stackArray[top--];
	}
	
		// peek at top of stack
	public char peek()
	{
		return stackArray[top];
	}
	
		// true if stack is empty
	public boolean isEmpty()
	{
		return top == -1;
	}
	
		// true if stack is full
	public boolean isFull()
	{
		return top == maxSize -1;
	}
}//end class StackY
