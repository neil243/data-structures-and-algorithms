//************************************************
// Author: Neil Kasanda
//
// Program: class that implement a stack
//************************************************

public class StackZ 
{
		// instance variables
	private int maxSize;
	private char[] stackArray;
	private int top;
	
		// constructor
	public StackZ(int size)
	{
		maxSize = size;
		stackArray = new char[maxSize];
		top = -1;
	}
	
		// push item on top of stack
	public void push(char j)
	{
		top++;
		stackArray[top] = j;
	}
	
		// take item from top of stack
	public char pop()
	{
		return stackArray[top--];
	}
	
		// peek at top of stack
	public char peek()
	{
		return stackArray[top];
	}
	
		// true if stack is empty
	public boolean isEmpty()
	{
		return top == -1;
	}
	
		// true if stack is full
	public boolean isFull()
	{
		return top == maxSize - 1;
	}
}// end class StackZ