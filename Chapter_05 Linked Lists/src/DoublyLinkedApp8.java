//***************************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class DoublyLinkedList8
// Listing 5.8
//***************************************************************

public class DoublyLinkedApp8
{
	public static void main(String[] args)
	{
		DoublyLinkedList8 theList = new DoublyLinkedList8();
		
			// insert at front
		theList.insertFirst(22);
		theList.insertFirst(44);
		theList.insertFirst(66);
		
			// insert at rear
		theList.insertLast(11);
		theList.insertLast(33);
		theList.insertLast(55);
		
			// display list forward
		theList.displayForward();
		
			// display list backward
		theList.displayBackward();
		
		theList.deleteFirst();					// delete first item
		theList.deleteLast();					// delete last item
		theList.deleteKey(11);					// delete item with key 11
		
			// display list forward
		theList.displayForward();
		
			// insert 77 after 22
		theList.insertAfter(22, 77);
			// insert 88 after 33
		theList.insertAfter(33, 88);
		
			// display list forward
		theList.displayForward();
	}//end main
}//end class DoublyLinkedApp