//********************************************************
// Author: Neil Kasanda
//
// Program: this class demonstrates a doubly-linked list
// Listing 5.8
//********************************************************

public class DoublyLinkedList8 
{
		// instance variables
	public Link8 first;
	public Link8 last;
	
		// constructor
	public DoublyLinkedList8()
	{
		first = last = null;
	}
	
		// true if no links
	public boolean isEmpty()
	{
		return first == null;
	}
	
		// insert at front of list
	public void insertFirst(long dd)
	{
		Link8 newLink = new Link8(dd);
		
		if(isEmpty())
			last = newLink;
		else
			first.previous = newLink;
		
		newLink.next = first;
		first = newLink;
	}
	
		// insert at end of list
	public void insertLast(long dd)
	{
		Link8 newLink = new Link8(dd);
		
		newLink.previous = last;
		last = newLink;
		
		if(isEmpty())
			first = newLink;
		else
			last.previous.next = newLink;
	}
	
		// delete first link (assumes non-empty list)
	public Link8 deleteFirst()
	{
		Link8 temp = first;
		
		if(first.next == null)
			last = null;
		else
			first.next.previous = null;
		first = first.next;
		
		temp.next = null;
		return temp;
	}
	
		// delete last link (assumes non-empty list)
	public Link8 deleteLast()
	{
		Link8 temp = last;
		
		if(first == last)
			first = null;
		
		else
			last.previous.next = null;
		last = last.previous;
		
		temp.previous = null;
		return temp;
	}
	
	
	public boolean insertAfter(long key, long dd)
	{		
		Link8 current = first;
		
		while(current.dData != key)
		{
			current = current.next;
			if(current == null)
				return false;
		}
		
		Link8 newLink = new Link8(dd);
		
		if(current == last)
			last = newLink;
		else
		{
			newLink.next = current.next;
			current.next.previous = newLink;
		}
		
		current.next = newLink;
		newLink.previous = current;
		return true;
	}
	
		// deletes item with given key
		// (assumes non-empty list)
	public Link8 deleteKey(long key)
	{
		Link8 current = first;
		
		while(current.dData != key)
		{
			current = current.next;
			if(current == null)
				return null;
		}
		
		if(current == first)
			first = current.next;
		else
			current.previous.next = current.next;
		
		if(current == last)
			last = current.previous;
		else
			current.next.previous = current.previous;
		return current;
	}
	
	public void displayForward()
	{
		System.out.print("List (first-->last): ");
		Link8 current = first;
		while(current != null)
		{
			current.displayLink();
			current = current.next;
		}
		System.out.println();
	}
	
	public void displayBackward()
	{
		System.out.print("List (last--> first): ");
		Link8 current = last;
		while(current != null)
		{
			current.displayLink();
			current = current.previous;
		}
		System.out.println();
	}
}//end class DoublyLinkedList8