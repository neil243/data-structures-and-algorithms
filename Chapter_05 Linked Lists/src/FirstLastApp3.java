//***********************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class FirstLastList
// Listing 5.3
//***********************************************************

public class FirstLastApp3 
{
	public static void main(String[] args)
	{
		FirstLastList3 theList = new FirstLastList3();				// make new list
		
			// insert at front
		theList.insertFirst(22);
		theList.insertFirst(44);
		theList.insertFirst(66);
		
			// insert at rear
		theList.insertLast(11);
		theList.insertLast(33);
		theList.insertLast(55);
		
			// display the list
		theList.displayList();
		
			// delete the first two items
		theList.deleteFirst();
		theList.deleteFirst();
		
			// display again
		theList.displayList();
	}//end main()
}//end FirstLastApp
