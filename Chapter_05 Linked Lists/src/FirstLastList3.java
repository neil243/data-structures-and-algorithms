//***********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a double-ended linked-list
// with first and last references
// Listing 5.3
//***********************************************************

public class FirstLastList3
{
		// instance variables
	private Link3 first;
	private Link3 last;
	
		// constructor
	public FirstLastList3()
	{
		first = null;					// no links on list yet
		last = null;
	}
	
		// true if no links
	public boolean isEmpty()
	{
		return first == null;
	}
	
		// insert in front of list
	public void insertFirst(long dd)
	{
		Link3 newLink = new Link3(dd);
		
		if(isEmpty())
			last = newLink;
		
		newLink.next = first;
		first = newLink;
	}
	
		// insert at end of list
	public void insertLast(long dd)
	{
		Link3 newLink = new Link3(dd);
		
		if(isEmpty())
			first = newLink;
		else
			last.next = newLink;
		last = newLink;
	}
	
		// delete first link (assumes non-empty list)
	public long deleteFirst()
	{
		long temp = first.dData;
		
		first = first.next;
		if(first == null)
			last = null;
		return temp;
	}
	
	public void displayList()
	{
		System.out.print("List (first-->last): ");
		Link3 current = first;
		while(current != null)
		{
			current.displayLink();
			current = current.next;
		}
		System.out.println();
		
	}
}// end class FirstLastList