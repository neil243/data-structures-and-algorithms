//**********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a double-ended linked-list
// Listing 5.5
//**********************************************************

public class FirstLastList5 
{
		// instance variables
	private Link5 first;					// ref to first item
	private Link5 last;						// ref to last item
	
		// constructor
	public FirstLastList5()
	{
		first = null;
		last = null;
	}
	
		// true if no links
	public boolean isEmpty()
	{
		return first == null;
	}
	
		// insert at end of list
	public void insertLast(long dd)
	{
		Link5 newLink = new Link5(dd);
		
		if(isEmpty())
			first = newLink;
		else
			last.next = newLink;
		last = newLink;
	}
	
		// delete first link
	public long deleteFirst()
	{
		Link5 temp = first;
		
		first = first.next;
		if(first == null)
			last = null;
		
		temp.next = null;
		return temp.dData;
	}
	
	public void displayList()
	{
		Link5 current = first;
		while(current != null)
		{
			current.displayLink();
			current = current.next;
		}
		System.out.println();
	}
}//end class FirstLastLink