//***************************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class ListIterator9
// Listing 5.9
//***************************************************************

import java.io.*;

public class InterIterApp9
{
	public static void main(String[] args) throws IOException
	{
		LinkList9 theList = new LinkList9();			// new list
		ListIterator9 iter1 = theList.getIterator();		// new iterator
		long value;
		
			// insert items
		iter1.insertAfter(20);
		iter1.insertAfter(40);
		iter1.insertAfter(80);
		iter1.insertBefore(60);
		
		while(true)
		{
			System.out.print("Enter first letter of show, rest, ");
			System.out.print("next, get, before, after, delete: ");
			int choice = getChar();						// get user's option
			
			if(choice == '\u0000')
				break;
			
			switch(choice)
			{
			case 's':									// show list
				if(!theList.isEmpty())
					theList.displayList();
				else
					System.out.println("The list is empty");
				break;
				
			case 'r':									// reset (to first)
				iter1.reset();
				break;
				
			case 'n':									// advance to next link
				if(!theList.isEmpty() && ! iter1.atEnd())
					iter1.nextLink();
				else
					System.out.println("Cannot go to the next link");
				break;
				
			case 'g':									// get current item
				if(!theList.isEmpty())
				{
					value = iter1.getCurrent().dData;
					System.out.println("Returned " + value);
				}
				else
					System.out.println("List is empty");
				break;
				
			case 'b':									// insert before current
				System.out.print("Enter value to insert: ");
				value = getInt();
				iter1.insertBefore(value);
				break;
				
			case 'a':									// insert after current
				System.out.print("Enter value to insert: ");
				value = getInt();
				iter1.insertAfter(value);
				break;
				
			case 'd':									// delete current item
				if(!theList.isEmpty())
				{
					value = iter1.deleteCurrent();
					System.out.println("Deleted " + value);
				}
				else
				{
					System.out.println("Cannot delete");
				}
				break;
				
				default:
					System.out.println("Invalid entry");
			}//end switch
		}//end while
	}//end main
	
	public static String getString() throws IOException
	{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		return s;
	}
	
	public static int getChar() throws IOException
	{
		String s = getString();
		return s.charAt(0);
	}
	
	public static int getInt() throws IOException
	{
		String s = getString();
		return Integer.parseInt(s);
	}
}//end class InterIterApp9