//********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a link of a linked-list
//********************************************************

public class Link 
{
		// instance variables
	public int iData;				// data item (key)
	public double dData;			// data item
	public Link next;				// next link in list
	
		// constructor
	public Link(int id, double dd)
	{
		iData = id;
		dData = dd;
	}
	
	public void displayLink()		// method to display a link
	{
		System.out.print("{" + iData + ", " + dData + "} ");
	}
}//end class Link
