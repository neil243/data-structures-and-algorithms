//********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a link of a linked-list
// Listing 5.2
//********************************************************

public class Link2 
{
		// instance variables
	public int iData;
	public double dData;
	public Link2 next;
	
		// constructor
	public Link2(int id, double dd)
	{
		iData = id;
		dData = dd;
	}
	
		// display link data
	public void displayLink()
	{
		System.out.print("{" + iData + ", " + dData + "}");
	}
}//end class Link2
