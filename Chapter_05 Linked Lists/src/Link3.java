//********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a link of a linked-list
// Listing 5.3
//********************************************************

public class Link3 
{
		// instance variables
	public long dData;
	public Link3 next;
	
		// constructor
	public Link3(long d)
	{
		dData = d;
	}
	
	public void displayLink()
	{
		System.out.print("{" + dData + "} ");
	}
}// end class Link3