//********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a link of a linked-list
// Listing 5.4
//********************************************************

public class Link4
{
		// instance variables
	public long dData;
	public Link4 next;
	
		// constructor
	public Link4(long dd)
	{
		dData = dd;
	}
	
	public void displayLink()
	{
		System.out.print("{" + dData + "} ");
	}
}//end class Link4