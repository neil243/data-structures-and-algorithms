//********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a link of a linked-list
// Listing 5.5
//********************************************************

public class Link5
{
		// instance variables
	public long dData;
	public Link5 next;
	
		// constructor
	public Link5(long dd)
	{
		dData = dd;
	}
	
		// display the link
	public void displayLink()
	{
		System.out.print("{" + dData + "} ");
	}
}//end class Link5
