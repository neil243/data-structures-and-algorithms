//********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a link of a linked-list
// Listing 5.6
//********************************************************

public class Link6
{
		// instance variables
	public long dData;
	public Link6 next;
	
		// constructor
	public Link6(long dd)
	{
		dData = dd;
	}
	
		// display this link
	public void displayLink()
	{
		System.out.print("{" + dData + "} ");
	}
}//end class Link6