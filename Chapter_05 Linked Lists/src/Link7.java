//********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a link of a linked-list
// Listing 5.7
//********************************************************

public class Link7
{
		// instance variables
	public long dData;
	public Link7 next;
	
		// constructor
	public Link7(long dd)
	{
		dData = dd;
	}
	
		// display this link
	public void displayLink()
	{
		System.out.print("{" + dData + "} ");
	}
}//end class Link7