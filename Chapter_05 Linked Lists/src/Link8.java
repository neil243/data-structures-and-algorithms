//***************************************************************
// Author: Neil Kasanda
//
// Program: this class implements a link of a doubly-linked list
// Listing 5.8
//***************************************************************

public class Link8 
{
		// instance variables
	public long dData;
	public Link8 next;
	public Link8 previous;
	
		// constructor
	public Link8(long dd)
	{
		dData = dd;
	}
	
		// display this link
	public void displayLink()
	{
		System.out.print("{" + dData + "} ");
	}
}//end class Link8
