//***************************************************************
// Author: Neil Kasanda
//
// Program: this class implements a link of a linked list
// Listing 5.9
//***************************************************************

public class Link9
{
		// instance variables
	public long dData;
	public Link9 next;
	
		// constructor
	public Link9(long dd)
	{
		dData = dd;
	}
	
		// display this link
	public void displayLink()
	{
		System.out.print("{" + dData + "} ");
	}
}//end class Link9