//********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a linked-list
//********************************************************

public class LinkList 
{
		// instance variables
	private Link first;						// ref to first link on list
	
		// constructor
	public LinkList()
	{
		first = null;
	}
	
		// true if list is empty
	public boolean isEmpty()
	{
		return first == null;
	}
	
		// insert at start of list
	public void insertFirst(int id, double dd)
	{
		Link newLink = new Link(id, dd);	// make new link
		newLink.next = first;				// newLink --> first (old)
		first = newLink;					// first --> newLink
	}
	
		// delete first item (assumes list not empty)
	public Link deleteFirst()
	{
		Link temp = first;					// save reference to link
		first = first.next;					// delete old first
		
		temp.next = null;
		return temp;						// return deleted link
	}
	
	public void displayList()
	{
		System.out.print("List (first-->last): ");
		Link current = first;
		while(current != null)
		{
			current.displayLink();			// print data
			current = current.next;			// move to next link
		}
		System.out.println();
	}
}//end class LinkList