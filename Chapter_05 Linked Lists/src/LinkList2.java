//********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a linked-list
// Listing 5.2
// Here we added search and delete operations.
//********************************************************

public class LinkList2 
{
		// instance variables
	private Link2 first;						// ref to first link on list
	
		// constructor
	public LinkList2()
	{
		first = null;						// no links on list yet
	}
	
	public void insertFirst(int id, double dd)
	{
		Link2 newLink = new Link2(id, dd);	// make new link
		newLink.next = first;				// it points to old first link
		first = newLink;					// now first points to this
	}
	
		// find link with given key (assumes non-empty list)
	public Link2 find(int key)
	{
		Link2 current = first;				// start at first
		while(current != null)
		{
			if(current.iData == key)
				return current;
			current = current.next;
		}
		
		return null;
	}
	
		// delete link with given key
	public Link2 delete(int key)
	{
		Link2 current = first;
		Link2 previous = current;
		
		while(current.iData != key)
		{
			if(current.next == null)
				return null;
			else
			{
				previous = current;
				current = current.next;
			}
		}
		
			// found it
		if(current == first)					// if first link, change first
			first = first.next;
		else									// otherwise,
			previous.next = current.next;		// bypass it
		
		return current;
	}
	
		// display the list
	public void displayList()
	{
		System.out.print("List (first-->last): ");
		
		Link2 current = first;					// start at the beginning of list
		while(current != null)					// until end
		{
			current.displayLink();				// print data
			current = current.next;				// move to next link
		}
		System.out.println();
	}
}//end class LinkList2