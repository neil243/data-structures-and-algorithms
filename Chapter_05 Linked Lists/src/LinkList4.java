//***********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a linked-list
// Listing 5.4
//***********************************************************

public class LinkList4 
{
		// instance variables
	private Link4 first;
	
		// constructor
	public LinkList4()
	{
		first = null;
	}
	
		// true if list is empty
	public boolean isEmpty()
	{
		return first == null;
	}
	
		// insert at start of list
	public void insertFirst(long dd)
	{
		Link4 newLink = new Link4(dd);				// make new link
		newLink.next = first;
		first = newLink;
	}
	
		// delete first item (assumes list not empty)
	public long deleteFirst()
	{
		Link4 temp = first;
		first = first.next;
		temp.next = null;
		return temp.dData;
	}
	
		// display entire list
	public void displayList()
	{
		Link4 current = first;
		while(current != null)
		{
			current.displayLink();
			current = current.next;
		}
		System.out.println();
	}
}//end class LinkList4