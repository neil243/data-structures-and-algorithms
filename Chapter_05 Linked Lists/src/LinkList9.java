//***************************************************************
// Author: Neil Kasanda
//
// Program: a class to demonstrate a linked-list with an iterator
// Listing 5.9
//***************************************************************

public class LinkList9
{
		// instance variables
	private Link9 first;
	
		// constructor
	public LinkList9()
	{
		first = null;							// no items on list yet
	}
	
		// get value of first
	public Link9 getFirst()
	{
		return first;
	}
	
		// set first to new link
	public void setFirst(Link9 f)
	{
		first = f;
	}
	
		// true if list is empty
	public boolean isEmpty()
	{
		return first == null;
	}
	
		// return iterator
	public ListIterator9 getIterator()
	{
		return new ListIterator9(this);			// initialized with this list
	}
	
	public void displayList()
	{
		Link9 current = first;
		while(current != null)
		{
			current.displayLink();
			current = current.next;
		}
		System.out.println();
	}
}//end class LinkList9
