//********************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class LinkList
//********************************************************

public class LinkListApp 
{
	public static void main(String[] args)
	{
		LinkList theList = new LinkList();			// make new list
		
			// insert four items
		theList.insertFirst(22, 2.99);
		theList.insertFirst(44, 4.99);
		theList.insertFirst(66, 6.99);
		theList.insertFirst(88, 8.99);
		
		theList.displayList();						// display list
		
			// until it's empty
		while(!theList.isEmpty())
		{
			Link aLink = theList.deleteFirst();		// delete link
			System.out.print("Deleted ");
			aLink.displayLink();					// display it
			System.out.println();
		}
		
		theList.displayList();						// display list
	}//end main
}//end class LinkListApp
