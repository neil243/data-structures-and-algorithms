//********************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class LinkList2
//********************************************************

public class LinkListApp2 
{
	public static void main(String[] args)
	{
		LinkList2 theList = new LinkList2();				// make the list
		
			// insert 4 items
		theList.insertFirst(22, 2.99);
		theList.insertFirst(44, 4.99);
		theList.insertFirst(66, 6.99);
		theList.insertFirst(88, 8.99);
		
		theList.displayList();								// display list
		
		Link2 f = theList.find(44);							// find item
		if(f != null)
			System.out.println("Found link with key " + f.iData);
		else
			System.out.println("Can't find link");
		
		Link2 d = theList.delete(66);
		if(d != null)
			System.out.println("Deleted link with key " + d.iData);
		else
			System.out.println("Can't delete link");
		
			// display list again
		theList.displayList();
	}//end main()
}// end class LinkListApp2