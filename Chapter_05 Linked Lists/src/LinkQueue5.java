//***********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a queue with a double-ended 
// linked-list
// Listing 5.5
//***********************************************************

public class LinkQueue5
{
		// instance variables
	FirstLastList5 theQueue;
	
		// constructor
	public LinkQueue5()
	{
		theQueue = new FirstLastList5();				// make a 2-ended list
	}
	
		// true if queue is empty
	public boolean isEmpty()
	{
		return theQueue.isEmpty();
	}
	
		// insert at rear of queue
	public void insert(long dd)
	{
		theQueue.insertLast(dd);
	}
	
		// remove front of queue
	public long remove()
	{
		return theQueue.deleteFirst();
	}
	
	public void displayQueue()
	{
		System.out.print("Queue (front-->rear): ");
		theQueue.displayList();
	}
}//end class LinkQueue