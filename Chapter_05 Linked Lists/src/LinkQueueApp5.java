//***********************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class LinkQueue
// Listing 5.5
//***********************************************************

public class LinkQueueApp5 
{
	public static void main(String[] args)
	{
		LinkQueue5 theQueue = new LinkQueue5();
		
			// insert items
		theQueue.insert(20);
		theQueue.insert(40);
		
			// display queue
		theQueue.displayQueue();
		
			// insert items
		theQueue.insert(60);
		theQueue.insert(80);
		
			// display queue
		theQueue.displayQueue();
		
			// remove items
		theQueue.remove();
		theQueue.remove();
		
			// display queue
		theQueue.displayQueue();
	}//end main
}//end class LinkQueueApp5