//***********************************************************
// Author: Neil Kasanda
//
// Program: this class implements a stack with a linked-list
// Listing 5.4
//***********************************************************

public class LinkStack4
{
		// instance variables
	private LinkList4 theList;
	
		// constructor
	public LinkStack4()
	{
		theList = new LinkList4();
	}
	
		// push item on top of stack
	public void push(long j)
	{
		theList.insertFirst(j);
	}
	
		// take item on top of stack
	public long pop()
	{
		return theList.deleteFirst();
	}
	
		// true if stack is empty
	public boolean isEmpty()
	{
		return theList.isEmpty();
	}
	
	public void displayStack()
	{
		System.out.print("Stack (top-->bottom): ");
		theList.displayList();
	}
}//end class LinkStack