//***********************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class LinkStack
// Listing 5.4
//***********************************************************

public class LinkStackApp4 
{
	public static void main(String[] args)
	{
		LinkStack4 theStack = new LinkStack4();	// make stack
		
			// push items
		theStack.push(20);
		theStack.push(40);
		
			// display stack
		theStack.displayStack();
		
			// push items
		theStack.push(60);
		theStack.push(80);
		
			// display stack
		theStack.displayStack();
		
			// pop items
		theStack.pop();
		theStack.pop();
		
			// display stack
		theStack.displayStack();
	}//end main()
}//end class LinkStackApp