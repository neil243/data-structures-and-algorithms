//***************************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class SortedList7
// Listing 5.7
//***************************************************************

public class ListInsertionSortApp7
{
	public static void main(String[] args)
	{
		int size = 10;
		
			// create array of links
		Link7[] linkArr = new Link7[size];
		
		for(int j = 0; j < size; j++)
		{
			int n = (int)(Math.random() * 99);
			Link7 newLink = new Link7(n);
			linkArr[j] = newLink;
		}
		
			// display array contents
		System.out.print("Unsorted Array:   ");
		for(int j = 0; j < size; j++)
			linkArr[j].displayLink();
		System.out.println();
		
			// create new list initialized with array
		SortedList7 theSortedList = new SortedList7(linkArr);
		
			// links from list to array
		for(int j = 0; j < size; j++)
			linkArr[j] = theSortedList.remove();
		
			// display array contents
		System.out.print("Sorted Array:   ");
		for(int j = 0; j < size; j++)
			linkArr[j].displayLink();
		System.out.println();
	}//end main()
}//end class ListInsertionSortApp7