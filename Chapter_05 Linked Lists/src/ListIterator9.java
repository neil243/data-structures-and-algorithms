//**************************************
// Author: Neil Kasanda
//
// Program: iterator class for LinkList9
// Listing 5.9
//**************************************

public class ListIterator9
{
		// instance variables
	private Link9 previous;
	private Link9 current;
	private LinkList9 ourList;
	
		// constructor
	public ListIterator9(LinkList9 list)
	{
		ourList = list;
		reset();
	}
	
		// start at 'first'
	public void reset()
	{
		current = ourList.getFirst();
		previous = null;
	}
	
		// true if last link
	public boolean atEnd()
	{
		return current.next == null;
	}
		// go to next link
	public void nextLink()
	{
		previous = current;
		current = current.next;
	}
	
		// get current link
	public Link9 getCurrent()
	{
		return current;
	}
	
	public void insertAfter(long dd)
	{
		Link9 newLink = new Link9(dd);
		
		if(ourList.isEmpty())
		{
			ourList.setFirst(newLink);
			current = newLink;
		}
		else
		{
			newLink.next = current.next;
			current.next = newLink;
			nextLink();
		}
	}
	
	public void insertBefore(long dd)
	{
		Link9 newLink = new Link9(dd);
		
			// beginning of list or empty list
		if(previous == null)
		{
			newLink.next = current;
			ourList.setFirst(newLink);
			reset();
		}
			// not beginning
		else
		{
			newLink.next = current;
			previous.next = newLink;
			current = newLink;
		}
	}
	
	public long deleteCurrent()
	{
		long value = current.dData;
		
			// beginning of list
		if(previous == null)
		{
			ourList.setFirst(current.next);
			reset();
		}
			// not beginning
		else
		{
			previous.next = current.next;
			if(atEnd())
				reset();
			else
				current = current.next;
		}
		
		return value;
	}
}//end class ListIterator9