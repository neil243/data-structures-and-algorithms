//***************************************************************
// Author: Neil Kasanda
//
// Program: this class implements and demonstrates a sorted list
// Listing 5.6
//***************************************************************

public class SortedList6 
{
		// instance variables
	private Link6 first;
	
		// constructor
	public SortedList6()
	{
		first = null;
	}
	
		// true if no links
	public boolean isEmpty()
	{
		return first == null;
	}
	
	public void insert(long key)
	{
		Link6 newLink = new Link6(key);
		Link6 previous = null;
		Link6 current = first;
		
		while(current != null && key > current.dData)
		{
			previous = current;
			current = current.next;
		}
		
		newLink.next = current;
		
			// at beginning of list
		if(previous == null)
			first = newLink;
		else
			previous.next = newLink;
	}//end insert
	
		// delete and return first link (assumes non-empty list)
	public Link6 remove()
	{
		Link6 temp = first;
		first = first.next;
		temp.next = null;
		return temp;
	}
	
	public void displayList()
	{
		Link6 current = first;
		
		while(current != null)
		{
			current.displayLink();					// print data
			current = current.next;					// move to next link
		}
		System.out.println();
	}
}//end class SortedList6
