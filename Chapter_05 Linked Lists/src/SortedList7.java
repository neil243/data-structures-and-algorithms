//***************************************************************
// Author: Neil Kasanda
//
// Program: demonstrates sorted linked-list used for sorting
// Listing 5.7
//***************************************************************

public class SortedList7 
{
		// instance variable
	public Link7 first;
	
		// default constructor
	public SortedList7()
	{
		first = null;
	}
	
		// constructor with array as argument
	public SortedList7(Link7[] linkArr)
	{
		first = null;
		for(int j = 0; j < linkArr.length; j++)
			insert(linkArr[j]);
	}
		// insert (in order)
	public void insert(Link7 k)
	{
		Link7 previous = null;
		Link7 current = first;
		
		while(current != null && current.dData < k.dData)
		{
			previous = current;
			current = current.next;
		}
		
		k.next = current;
		
			// if at the beginning of list
		if(previous == null)
			first = k;
		else
			previous.next = k;
	}//end insert
	
	public Link7 remove()
	{
		Link7 temp = first;
		first = first.next;
		temp.next = null;
		return temp;
	}
}//end class SortedList7