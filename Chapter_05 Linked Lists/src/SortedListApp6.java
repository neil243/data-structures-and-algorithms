//***************************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class SortedList6
// Listing 5.6
//***************************************************************

public class SortedListApp6
{
	public static void main(String[] args)
	{
		SortedList6 theSortedList = new SortedList6();			// create new list
		
			// insert 2 items
		theSortedList.insert(20);
		theSortedList.insert(40);
		
			// display the list
		theSortedList.displayList();
		
			// insert 3 more items
		theSortedList.insert(10);
		theSortedList.insert(30);
		theSortedList.insert(50);
		
			// display the list
		theSortedList.displayList();
		
			// remove an item
		theSortedList.remove();
		
			// display list
		theSortedList.displayList();
	}//end main
}//end class SortedListApp6