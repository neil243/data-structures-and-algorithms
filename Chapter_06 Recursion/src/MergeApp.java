//********************************************************
// Author: Neil Kasanda
//
// Program: demonstrates merging two arrays into a third
//********************************************************

public class MergeApp
{
	public static void main(String[] args)
	{
		int[] arrayA = {23, 47, 81, 95};
		int[] arrayB = {7, 14, 39, 55, 62, 74};
		int[] arrayC = new int[arrayA.length + arrayB.length];
		
		merge(arrayA, 4, arrayB, 6, arrayC);
		display(arrayC, 10);
	}
	
	public static void merge(int[] arrayA, int sizeA, 
			int[] arrayB, int sizeB, int[] arrayC)
	{
		int idxA = 0, idxB = 0, idxC = 0;
		
		while(idxA < sizeA && idxB < sizeB)
			if(arrayA[idxA] < arrayB[idxB])
				arrayC[idxC++] = arrayA[idxA++];
			else
				arrayC[idxC++] = arrayB[idxB++];
		
		while(idxA < sizeA)									// arrayB is empty
			arrayC[idxC++] = arrayA[idxA++];				// but arrayA isn't
		
		while(idxB < sizeB)									// arrayA is empty,
			arrayC[idxC++] = arrayB[idxB++];				// but arrayB isn't
	}//end merge
	
	public static void display(int[] array, int size)
	{
		for(int j = 0; j < size; j++)
		{
			System.out.print(array[j] + " ");
		}
		System.out.println();
	}
}