//*******************************************************
// Author: Neil Kasanda
//
// Program: demonstrates recursive binary search
//*******************************************************

public class OrdArray
{
		// instance variables
	private long[] a;										// ref to array
	private int nElems;										// no of data items
	
		// constructor
	public OrdArray(int size)
	{
		a = new long[size];
		nElems = 0;
	}
	
	public int size()
	{
		return nElems;
	}
	
	public int find(long searchKey)
	{
		return recFind(searchKey, 0, nElems - 1);
	}
	
	public int recFind(long searchKey, int lowerBound, int upperBound)
	{
		int mid;
		
		mid = (lowerBound + upperBound) / 2;
		
		if(a[mid] == searchKey)									// found it
			return mid;
		else if(lowerBound > upperBound)						// cannot find it
			return nElems;
		else													// divide range
		{
			if(a[mid] < searchKey)								// it's in upper half
				return recFind(searchKey, mid + 1, upperBound);
			else												// it's in lower half
				return recFind(searchKey, lowerBound, mid - 1);
		}
	}
	
	public void insert(long value)
	{
		int j;
		
		for(j = 0; j < nElems; j++)
			if(a[j] > value)
				break;
		
		for(int k = nElems; k > j; k--)
			a[k] = a[k - 1];
		
		a[j] = value;
		
		nElems++;
	}//end insert()
	
		// display array contents
	public void display()
	{
		for(int j = 0; j < nElems; j++)							// for each element,
			System.out.print(a[j] + " ");						// display it.
		System.out.println();
	}
}// end class OrdArray