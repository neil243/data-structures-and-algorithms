//*************************************************
// Author: Neil Kasanda
//
// Program: evaluates triangular numbers, stack
// replaces recursion
// Listing 6.7
//*************************************************

public class Params
{
		// parameters to save on stack
	public int n;
	public int returnAddress;
	
	public Params(int nn, int ra)
	{
		n = nn;
		returnAddress = ra;
	}
}//end class Params