//*************************************************************
// Author: Neil Kasanda
//
// Program: my own version of the anagram program
//*************************************************************
public class Permutations
{
	static int count;
	
	public static void main(String[] args)
	{
		count = 0;
		
		doPermutation("", "count");
	}
	
	public static void doPermutation(String perm, String word)
	{
		if(word.length() == 0)
		{
			count++;
			System.out.printf("%3d %s     ", count, perm);
			if(count % 6 == 0)
				System.out.println();
		}
		else
		{
			for(int j = 0; j < word.length(); j++)
			{
				doPermutation(perm + word.charAt(j), word.substring(0, j) + word.substring(j + 1));
			}
		}
	}
}
