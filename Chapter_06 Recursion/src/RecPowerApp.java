//**********************************************************
// Author: Neil Kasanda
//
// Program: demonstrates how to compute a number to a given
// power recursively
// Listing 6.8.1 left as an exercise
//**********************************************************

public class RecPowerApp
{
	public static void main(String[] args) 
	{
		System.out.println("3 to the power of 3 = " + power(2, 4));
	}
	
	public static int power(int x, int y)
	{
		if(y == 1)
			return x;
		else
		{
			if(y % 2 == 1)
			{
				return x * power(x * x, y / 2);
			}
			return power(x * x, y / 2);
		}
	}
}