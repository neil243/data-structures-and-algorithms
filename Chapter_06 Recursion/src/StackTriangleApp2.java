//********************************************************
// Author: Neil Kasanda
//
// Program: replaces step method of StackTriangleApp with
// two loops
// Listing 6.8
//********************************************************

import java.io.*;

public class StackTriangleApp2
{
	static int theNumber;
	static int theAnswer;
	static StackY theStack;
	
	public static void main(String[] args) throws IOException
	{
		System.out.print("Enter a number: ");
		theNumber = getInt();
		stackTriangle();
		System.out.println("Triangle = " + theAnswer);
	}
	
	public static void stackTriangle()
	{
		theStack = new StackY(1000);
		
		while(theNumber > 0)
		{
			theStack.push(theNumber);
			theNumber--;
		}
		
		while(!theStack.isEmpty())
		{
			theAnswer = theAnswer + theStack.pop();
		}
	}
	
	public static String getString() throws IOException
	{
		InputStreamReader isr = new InputStreamReader(System.in);
		BufferedReader br = new BufferedReader(isr);
		String s = br.readLine();
		return s;
	}
	
	public static int getInt() throws IOException
	{
		String s = getString();
		return Integer.parseInt(s);
	}
}
