//*************************************************
// Author: Neil Kasanda
//
// Program: evaluates triangular numbers, stack
// replaces recursion
// Listing 6.7
//*************************************************

public class StackX
{
	private int maxSize;					// size of StackX array
	private Params[] stackArray;
	private int top;						// top of stack
	
	public StackX(int s)
	{
		maxSize = s;						// set array size
		stackArray = new Params[maxSize];	// create array
		top = -1;							// no items yet
	}
	
		// push item on top of stack
	public void push(Params p)
	{
		top++;								// increment top
		stackArray[top] = p;				// insert item
	}
	
		// take item from top of stack
	public Params pop()
	{
		return stackArray[top--];			// access item, decrement top
	}
	
		// peek at top of stack
	public Params peek()
	{
		return stackArray[top];
	}
	
		// true if stack is empty
	public boolean isEmpty()
	{
		return top == -1;
	}
}//end class StackX