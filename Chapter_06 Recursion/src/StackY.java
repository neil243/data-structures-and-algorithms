//***************************************************
// Author: Neil Kasanda
//
// Program: stack implementation
// Listing 6.8
//***************************************************

public class StackY
{
	private int size;
	private int[] a;
	private int top;
	
	public StackY(int s)
	{
		size = s;
		a = new int[size];
		top = -1;
	}
	
	public void push(int value)
	{
		top++;
		a[top] = value;
	}
	
	public int pop()
	{
		return a[top--];
	}
	
	public int peek()
	{
		return a[top];
	}
	
	public boolean isEmpty()
	{
		return top == -1;
	}
}