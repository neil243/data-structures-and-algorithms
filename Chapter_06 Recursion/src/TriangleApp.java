//********************************************************
// Author: Neil Kasanda
//
// Program: evaluates triangular numbers
// Listing 6.1
//********************************************************

import java.util.*;

public class TriangleApp
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int theNumber;
		int theAnswer;
		
		System.out.print("Enter a number: ");
		theNumber = console.nextInt();
		System.out.println();
		
		theAnswer = triangleDebug(theNumber);
		
		System.out.println("Triangle = " + theAnswer);
	}// end main
	
	public static int triangle(int n)
	{
		if(n == 1)
			return 1;
		else
			return n + triangle(n - 1);
	}
	
	public static int triangleDebug(int n)
	{
		System.out.println("Entering: n = " + n);
		
		if(n == 1)
		{
			System.out.println("Returning 1");
			return 1;
		}
		else
		{
			int temp = n + triangleDebug(n - 1);
			System.out.println("Returning " + temp);
			return temp;
		}
	}
}//end class TriangleApp