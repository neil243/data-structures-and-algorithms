//*********************************************
// Author: Neil Kasanda
//
// Program: demonstrates partitioning an array
// Listing 7.2
//*********************************************

public class ArrayPar
{
	private long[] theArray;						// ref to array theArray
	private int nElems;								// number of data items
	
		// constructor
	public ArrayPar(int size)
	{
		theArray = new long[size];
		nElems = 0;
	}
	
		// put element into array
	public void insert(long value)
	{
		theArray[nElems] = value;					// insert it
		nElems++;									// increment size
	}
	
		// return number of items
	public int size()
	{
		return nElems;
	}
	
		// display array contents
	public void display()
	{
		System.out.println("Displaying Array...");
		for(int j = 0; j < nElems; j++)
			System.out.print(theArray[j] + " ");
		System.out.println();
	}
	
	public int partitionIt(int left, int right, long pivot)
	{
		int leftPtr = left - 1;							// right of first element
		int rightPtr = right + 1;						// left of pivot
		
		while(true)
		{
			while(leftPtr < right && theArray[++leftPtr] < pivot)	// find bigger item
				; // (nop)
			
			while(rightPtr > left && theArray[--rightPtr] > pivot)	// find smaller item
				; // (nop)
			
			if(leftPtr >= rightPtr)									// if pointers cross,
				break;												// 		partition done
			else													// not crossed, so
				swap(leftPtr, rightPtr);							// 		swap elements
		}//end while(true)
		
		return leftPtr;												// return partition
		
	}//end partitionIt()
	
	public void swap(int dex1, int dex2)							// swap two elements
	{
		long temp;
		temp = theArray[dex1];										// A into temp
		theArray[dex1] = theArray[dex2];							// B into A
		theArray[dex2] = temp;										// temp into B
	}//end swap
}