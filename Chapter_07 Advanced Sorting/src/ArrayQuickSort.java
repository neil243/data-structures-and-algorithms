//****************************************************
// Author: Neil Kasanda
//
// Program: demonstrates simple version of quick sort
// Listing 7.3
//****************************************************

public class ArrayQuickSort
{
		// instance variables
	private long[] theArray;
	private int nElems;
	
		// constructor
	public ArrayQuickSort(int size)
	{
		theArray = new long[size];
		nElems = 0;
	}
	
		// put element into array
	public void insert(long value)
	{
		theArray[nElems] = value;
		nElems++;
	}
	
		// display array contents
	public void display()
	{
		System.out.println("Displaying Array...");
		for(int j = 0; j < nElems; j++)
			System.out.print(theArray[j] + " ");
		System.out.println();
	}
	
	public void recQuickSort(int left, int right)
	{
		if(right - left <= 0)
			return;
		else
		{
			long pivot = theArray[right];				// rightmost item
			
			int partition = partitionIt(left, right, pivot);
			recQuickSort(left, partition - 1);
			recQuickSort(partition + 1, right);
		}
	}//end recQuickSort()
	
	public int partitionIt(int left, int right, long pivot)
	{
		int leftPtr = left - 1;					// left (after ++)
		int rightPtr = right;					// right - 1 (after --)
		while(true)
		{
				// find bigger item
			while(theArray[++leftPtr] < pivot)
				; // (nop)
			
				// find smaller item
			while(rightPtr > left && theArray[--rightPtr] > pivot)
				; // (nop)
			
			if(leftPtr >= rightPtr)						// if pointers cross,
				break;									// 		partition done
			else										// not crossed, so
				swap(leftPtr, rightPtr);				// 		swap elements
		}//end while(true)
		swap(leftPtr, right);
		return leftPtr;									// return pivot
	}//end paritionIt()									// return pivot location
	
		// swap two elements
	public void swap(int dex1, int dex2)
	{
		long temp = theArray[dex1];
		theArray[dex1] = theArray[dex2];
		theArray[dex2] = temp;
	}//end swap
}//end class ArrayQuickSort