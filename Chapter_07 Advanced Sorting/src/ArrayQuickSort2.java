//********************************************************************
// Author: Neil Kasanda
//
// Program: demonstrates quick sort with median-of-three partitioning
// Listing 7.4
//********************************************************************

public class ArrayQuickSort2
{
		// instance variables
	private long[] theArray;							// ref to array the array
	private int nElems;									// number of data items
	
	public ArrayQuickSort2(int size)
	{
		theArray = new long[size];
		nElems = 0;
	}
	
		// put element into array
	public void insert(long value)
	{
		theArray[nElems] = value;
		nElems++;
	}
	
		// display array contents
	public void display()
	{
		System.out.println("Displaying Array...");
		for(int j = 0; j < nElems; j++)
			System.out.print(theArray[j] + " ");
		System.out.println();
	}
	
	public void quickSort()
	{
		recQuickSort(0, nElems - 1);
	}
	
	public void recQuickSort(int left, int right)
	{
		int size = right - left + 1;
		
		if(size <= 2)							// manual sort if small
			manualSort(left, right);
		else									// quick sort if large
		{
			long median = medianOf3(left, right);
			int partition = partitionIt(left, right, median);
			recQuickSort(left, partition - 1);
			recQuickSort(partition + 1, right);
		}
	}//end recQuickSort()
	
	public long medianOf3(int left, int right)
	{
		int center = (left + right) / 2;
		
			// order left and center
		if(theArray[left] > theArray[center])
			swap(left, center);
		
			// order left and right
		if(theArray[left] > theArray[right])
			swap(left, right);
		
			// order center and right
		if(theArray[center] > theArray[right])
			swap(center, right);
		
		swap(center, right - 1);					// put pivot on right
		return theArray[right - 1];					// return median value
	}//end medianOf3()
	
	public void swap(int dex1, int dex2)
	{
		long temp = theArray[dex1];
		theArray[dex1] = theArray[dex2];
		theArray[dex2] = temp;
	}//end swap()
	
	public int partitionIt(int left, int right, long pivot)
	{
		int leftPtr = left;								// left of first element
		int rightPtr = right - 1;						// left of pivot
		
		while(true)
		{
			while(theArray[++leftPtr] < pivot)	// find bigger item
				; // (nop)
			
			while(theArray[--rightPtr] > pivot)	// find smaller
				;
			
			if(leftPtr >= rightPtr)				// if pointers cross,
				break;							// 		partition done
			else								// not crossed,
				swap(leftPtr, rightPtr);		//		swap elements
		}// end while(true)
		swap(leftPtr, right - 1);				// restore pivot
		return leftPtr;
	}//end partitionIt()
	
	public void manualSort(int left, int right)
	{
		int size = right - left + 1;
		
		if(size <= 1)
			return;								// no sort necessary
		
			// 2-sort left and right
		if(size == 2)
		{
			if(theArray[left] > theArray[right])
				swap(left, right);
			return;
		}
			// size is 3
			// 3-sort left, center, right
		else
		{
				// left, center
			if(theArray[left] > theArray[right - 1])
				swap(left, right - 1);
			
				// left, right
			if(theArray[left] > theArray[right])
				swap(left, right);
			
				// center, right
			if(theArray[right - 1] > theArray[right])
				swap(right - 1, right);			
		}// end manualSort()
	}
}//end class ArrayQuickSort2