//*************************************************
// Author: Neil Kasanda
//
// Program: demonstrates quick sort; uses insertion sort
// for cleanup
// Listing 7.5
//*************************************************

public class ArrayQuickSort3
{
		// instance variables
	private long[] theArray;								// ref to array theArray
	private int nElems;										// number of data items
	
		// constructor
	public ArrayQuickSort3(int size)
	{
		theArray = new long[size];							// create the array
		nElems = 0;											// no items yet
	}
	
		// put element into array
	public void insert(long value)
	{
		theArray[nElems] = value;
		nElems++;
	}
	
		// display array contents
	public void display()
	{
		System.out.println("Displaying Array...");
		for(int j = 0; j < nElems; j++)
			System.out.print(theArray[j] + " ");
		System.out.println();
	}
	
	public void quickSort()
	{
		recQuickSort(0, nElems - 1);
		//insertionSort(0, nElems - 1); // the other option
	}
	
	public void recQuickSort(int left, int right)
	{
		int size = right - left + 1;
		if(size < 10)						// insertion sort if small
			insertionSort(left, right);
		else
		{
			long median = medianOf3(left, right);
			int partition = partitionIt(left, right, median);
			recQuickSort(left, partition - 1);
			recQuickSort(partition + 1, right);
		}
	}//end recQuickSort
	
	public long medianOf3(int left, int right)
	{
		int center = (right + left) / 2;
		
			// order left, center
		if(theArray[left] > theArray[center])
			swap(left, center);
		
			// order left, right
		if(theArray[left] > theArray[right])
			swap(left, right);
		
			// order center, right
		if(theArray[center] > theArray[right])
			swap(center, right);
		
		swap(center, right - 1);
		return theArray[right - 1];
	}// end medianOf3()
	
		// swap two elements
	public void swap(int dex1, int dex2)
	{
		long temp = theArray[dex1];
		theArray[dex1] = theArray[dex2];
		theArray[dex2] = temp;
	}//end swap()
	
	public int partitionIt(int left, int right, long pivot)
	{
		int leftPtr = left;
		int rightPtr = right - 1;
		
		while(true)
		{
			while(theArray[++leftPtr] < pivot)			// find bigger
				; // (nop)
			
			while(theArray[--rightPtr] > pivot)			// find smaller
				; // (nop)
			
			if(leftPtr >= rightPtr)						// if pointers cross,
				break;									//		partition done
			else										// not crossed,
				swap(leftPtr, rightPtr);				// 		swap elements
		}
		
		swap(leftPtr, right - 1);						// restore pivot
		return leftPtr;									// return pivot location
	}//end partitionIt()
	
	public void insertionSort(int left, int right)
	{
		int in, out;
		
		for(out = left + 1; out <= right; out++)
		{
			long temp = theArray[out];
			in = out;
			
			while(in > left && theArray[in - 1] > temp)
			{
				theArray[in] = theArray[in - 1];
				in--;
			}
			theArray[in] = temp;						// insert marked item
		}//end for
	}//end insertionSort()
}//end class ArrayQuickSort3()