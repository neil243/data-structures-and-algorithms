//***********************************
// Author: Neil Kasanda
//
// Program: demonstrates shell sort
// Listing 7.1
//***********************************

public class ArraySh
{
		// instance variables
	private long[] theArray;
	private int nElems;
	
		// constructor
	public ArraySh(int size)
	{
		theArray = new long[size];
		nElems = 0;
	}
	
		// put element into array
	public void insert(long value)
	{
		theArray[nElems] = value;
		nElems++;
	}
	
		// display array contents
	public void display()
	{
		System.out.println("Printing Array...");
		for(int j = 0; j < nElems; j++)			// for each element,
		{
			System.out.printf("%2d ", theArray[j]);	// display it.
			if((j + 1) % 50 == 0)
				System.out.println();
		}
		System.out.println();
	}
	
	public void shellSort()
	{
		int inner, outer;
		long temp;
		
		int gap = 1;							// find initial value of gap
		while(gap <= nElems / 3)
			gap = (gap * 3) + 1;				// (1, 4, 13, 40, 121, ...)
		
			// decrease gap, until gap = 1
		while(gap > 0)
		{
				// start at gap index, and step through all remaining array elements
			for(outer = gap; outer < nElems; outer++)
			{
				temp = theArray[outer];			// hold the value at gap index
				
					// variable to iterate from gap index backward
					// this value is decreased by gap amount
				inner = outer;
				
				while(inner >= gap && theArray[inner - gap] >= temp)
				{
					theArray[inner] = theArray[inner - gap];
					inner = inner - gap;
				}
				theArray[inner] = temp; 
			}
			gap = (gap - 1) / 3;					//decrease gap
		}//end while(gap > 0)
	}//end shellSort
}//end class ArraySh