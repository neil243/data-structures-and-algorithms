//**********************************************
// Author: Neil Kasanda
//
// Program: to test the operations of ArrayPar
// Listing 7.2
//**********************************************

public class PartitionApp
{
	public static void main(String[] args)
	{
		int maxSize = 3;						// array size
		ArrayPar arr = new ArrayPar(maxSize);	// create the array
		
		for(int j = 0; j < maxSize; j++)
		{
			long n = (int)(Math.random() * 199);
			arr.insert(n);
		}
		
			// display unsorted array
		arr.display();
		
		long pivot = 99;						// pivot value
		System.out.print("Pivot is " + pivot);
		int size = arr.size();
		
			// partition array
		int partDex = arr.partitionIt(0, size - 1, pivot);
		
		System.out.println(", Partition is at index " + partDex);
		
			// display partitioned array
		arr.display();
	}
}