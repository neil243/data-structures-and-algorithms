//*************************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class ArrayQuickSort
//*************************************************************

public class QuickSortApp1
{
	public static void main(String[] args)
	{
		int maxSize = 16;										// array size
		ArrayQuickSort arr = new ArrayQuickSort(maxSize);		// create array
		
		for(int j = 0; j < maxSize; j++)
		{
			long n = (int)(Math.random() * 99);
			arr.insert(n);
		}
		
			// display items
		arr.display();
		
			// quickSort it
		arr.recQuickSort(0, maxSize - 1);
		
			// display items again
		arr.display();
	}// end main
}// end class QuickSortApp1
