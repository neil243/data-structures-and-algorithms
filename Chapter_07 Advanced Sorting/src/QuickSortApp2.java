//**************************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class ArrayQuickSort2
// Listing 7.4
//**************************************************************

public class QuickSortApp2
{
	public static void main(String[] args)
	{
		int maxSize = 8;										// array size
		ArrayQuickSort2 arr = new ArrayQuickSort2(maxSize);		// create the array
		long n;
		
			// fill array with random numbers
		for(int j = 0; j < maxSize; j++)
		{
			n = (int)(Math.random() * 99);
			arr.insert(n);
		}
		
			// display items
		arr.display();
		
			// quickSort them
		arr.quickSort();
		
			// display them again
		arr.display();
	}
}//end class QuickSortApp2