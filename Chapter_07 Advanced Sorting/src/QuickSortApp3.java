//*************************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class ArrayQuickSort3
//*************************************************************

public class QuickSortApp3
{
	public static void main(String[] args)
	{
		int maxSize = 16;										// array size
		ArrayQuickSort3 arr = new ArrayQuickSort3(maxSize);		// create the array
		
			// fill array with random numbers
		for(int j = 0; j < maxSize; j++)
		{
			long n = (int)(Math.random() * 99);
			arr.insert(n);
		}
		
			// display items
		arr.display();
		
			// quick sort items
		arr.quickSort();
		
			// display items again
		arr.display();
	}//end main
}//end QuickSortApp3