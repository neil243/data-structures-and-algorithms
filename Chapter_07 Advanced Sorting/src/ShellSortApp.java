//******************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class ArraySh
// Listing 7.1
//******************************************************

public class ShellSortApp
{
	public static void main(String[] args)
	{
		int maxSize = 1000;						// array size
		ArraySh arr = new ArraySh(maxSize);		// create the array
		
		for(int j = 0; j < maxSize; j++)
		{
			long n = (int) (Math.random() * 99);
			arr.insert(n);
		}
		
			// display unsorted array
		arr.display();
		System.out.println();
		
			// shell sort the array
		arr.shellSort();
		
			// display sorted array
		arr.display();
		System.out.println();
	}//end main
}//end class ShellSortApp