//*******************************************************
// Author: Neil Kasanda
//
// Program: demonstrates node of binary tree
// Listing 8.1
//*******************************************************

public class Node
{
	public int iData;					// data item (key)
	public double dData;				// data item
	public Node leftChild;				// this node's left child
	public Node rightChild;				// this node's right child
	
		// constructor
	public Node(int id, double dd)
	{
		iData = id;
		dData = dd;
	}
	
		// display this Node
	public void displayNode()
	{
		System.out.print("{" + iData + ", " + dData + "}");
	}
}//end class Node