//*******************************************************
// Author: Neil Kasanda
//
// Program: demonstrates binary tree
// Listing 8.1
//*******************************************************

import java.util.*;

public class Tree
{
	private Node root;							// first node of tree
	
		// constructor
	public Tree()
	{
		root = null;							// no nodes in tree yet
	}
	
		// find Node with given key
	public Node find(int key)
	{
		Node current = root;					// start at root (assumes non-empty tree)
		while(current.iData != key)
		{
			if(key < current.iData)				// go left
				current = current.leftChild;
			else								// go right
				current = current.rightChild;
			
			if(current == null)					// if no child,
				return null;					// didn't find it
		}
		return current;							// found it
	}//end find()
	
	public void insert(int id, double dd)
	{
		Node newNode = new Node(id, dd);		// make new node with data
		
		if(root == null)
			root = newNode;
		else
		{
			Node current = root;				// start at root
			Node parent;
			while(true)
			{
				parent = current;
				if(id < current.iData)			// go left
				{
					current = current.leftChild;
					if(current == null)			// if end of line,
					{							// insert of left
						parent.leftChild = newNode;
						return;
					}
				}// end if go left
				else							// or go right
				{
					current = current.rightChild;
					if(current == null)
					{
						parent.rightChild = newNode;
						return;
					}
				}//end else go right
			}//end while
		}//end else root
	}//end insert()
	
		// delete node with given key (assumes non-empty tree)
	public boolean delete(int key)
	{
		Node current = root;
		Node parent = root;
		boolean isLeftChild = true;
		
		while(current.iData != key)			// search for node
		{
			parent = current;
			if(key < current.iData)
			{
				current = current.leftChild;
				isLeftChild = true;
			}
			else
			{
				current = current.rightChild;
				isLeftChild = false;
			}
			if(current == null)				// end of line
				return false;				// didn't find it
		}//end while
		
			//found node to delete
		
			// if no children, simply delete it
		if(current.leftChild == null && current.rightChild == null)
		{
			if(current == root)				// if root,
				root = null;				// tree is now empty
			
				// disconnect from parent
			else if(isLeftChild)
				parent.leftChild = null;			
			else
				parent.rightChild = null;
		}
		
			// if no right child, replace with left subtree
		else if(current.rightChild == null)
		{
			if(current == root)
				root = current.leftChild;
			else if(isLeftChild)
				parent.leftChild = current.leftChild;
			else
				parent.rightChild = current.leftChild;
		}
		
			// if no left child, replace with right subtree
		else if(current.leftChild == null)
		{
			if(current == root)
				root = current.rightChild;
			
			else if(isLeftChild)
				parent.leftChild = current.rightChild;
			else
				parent.rightChild = current.rightChild;
		}
		
			// two children, so replace with inorder successor
		else
		{
				// get successor of node to delete (current)
			Node successor = getSuccessor(current);
			
				// connect parent of current to successor
			if(current == root)
				root = successor;
			else if(isLeftChild)
				parent.leftChild = successor;
			else
				parent.rightChild = successor;
			
				// connect successor to current's left child
			successor.leftChild = current.leftChild;
		}//end else two children
		// (successor cannot have a left child. Impossible)
		
		return true; // success
	}//end delete()
	
		// returns node with next-highest value after delNode
		// goes to right child, the right child's left descendants if necessary
	private Node getSuccessor(Node delNode)
	{
		Node successorParent = delNode;
		Node successor = delNode;
		Node current = delNode.rightChild;
		while(current != null)
		{
			successorParent = successor;
			successor = current;
			current = current.leftChild;			// go to left child
		}
		
			// if successor is not right child, make connections
		if(successor != delNode.rightChild)
		{
			successorParent.leftChild = successor.rightChild;
			successor.rightChild = delNode.rightChild;
		}
		
		return successor;
	}
	
	public void traverse(int traverseType)
	{
		switch(traverseType)
		{
		case 1:
			System.out.print("\nPreorder traversal: ");
			preOrder(root);
			break;
		
		case 2:
			System.out.print("\nInorder traversal: ");
			inOrder(root);
			break;
		
		case 3:
			System.out.print("\nPostorder traversal: ");
			postOrder(root);
			break;
			
		default:
			System.out.print("\nInvalid traversal option");
		}
		System.out.println();
	}
	
	private void preOrder(Node node)
	{
		if(node == null)
			return;
		
		System.out.print(node.iData + " ");
		preOrder(node.leftChild);
		preOrder(node.rightChild);
	}
	
	private void inOrder(Node node)
	{
		if(node == null)
			return;
		
		inOrder(node.leftChild);
		System.out.print(node.iData + " ");
		inOrder(node.rightChild);
	}
	
	private void postOrder(Node node)
	{
		if(node == null)
			return;
		
		postOrder(node.leftChild);
		postOrder(node.rightChild);
		System.out.print(node.iData + " ");
	}
	
	public void displayTree()
	{
		Stack<Node> globalStack = new Stack<Node>();
		globalStack.push(root);
		int nBlanks = 32;
		boolean isRowEmpty = false;
		System.out.println("....................................."
				+ "..............................");
		
		while(!isRowEmpty)
		{
			Stack<Node> localStack = new Stack<Node>();
			isRowEmpty = true;
			
			for(int j = 0; j < nBlanks; j++)
				System.out.print(' ');
			
			while(!globalStack.isEmpty())
			{
				Node temp = (Node)globalStack.pop();
				if(temp != null)
				{
					System.out.print(temp.iData);
					localStack.push(temp.leftChild);
					localStack.push(temp.rightChild);
					
					if(temp.leftChild != null || temp.rightChild != null)
						isRowEmpty = false;
				}
				
				else
				{
					System.out.print("--");
					localStack.push(null);
					localStack.push(null);
				}
				
				for(int j = 0; j < nBlanks * 2 - 2; j++)
					System.out.print(' ');
			}//end while gobalStack not empty
			System.out.println();
			nBlanks /= 2;
			
			while(!localStack.isEmpty())
				globalStack.push(localStack.pop());
		}//end while isRowEmpty is false
		System.out.println("....................................."
				+ "..............................");
	}//end displayTree()
}//end class Tree