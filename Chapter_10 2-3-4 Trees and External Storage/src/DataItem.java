//**********************************************************
// Author: Neil Kasanda
//
// Program: demonstrates a data item in a node of a 234 tree
// Listing 10.1
//**********************************************************

public class DataItem
{
		// instance variables
	public long dData;						// one data item
	
		// constructor
	public DataItem(long dd)
	{
		dData = dd;
	}
	
		// display item, format /27
	public void displayItem()
	{
		System.out.print("/" + dData);
	}
}// end class DataItem