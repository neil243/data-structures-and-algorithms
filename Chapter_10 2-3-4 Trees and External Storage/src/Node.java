//**********************************************************
// Author: Neil Kasanda
//
// Program: demonstrates a node of a 234 tree
// Listing 10.1
//**********************************************************

public class Node
{
		// instance variables
	private static final int ORDER = 4;
	private int numItems;
	private Node parent;
	private Node[] childArray = new Node[ORDER];
	private DataItem[] itemArray = new DataItem[ORDER - 1];
	
		// No need for constructor, arrays are initialized to default
		// values
	
		// connect child to this Node
	public void connectChild(int childNum, Node child)
	{
		childArray[childNum] = child;
		if(child != null)
			child.parent = this;
	}
	
		// disconnect child from this node, return it
	public Node disconnectChild(int childNum)
	{
		Node tempNode = childArray[childNum];
		childArray[childNum] = null;
		return tempNode;
	}
	
	public Node getChild(int childNum)
	{
		return childArray[childNum];
	}
	
	public Node getParent()
	{
		return parent;
	}
	
	public boolean isLeaf()
	{
		return childArray[0] == null;
	}
	
	public int getNumItems()
	{
		return numItems;
	}//end getNumItems
	
	public DataItem getItem(int index)
	{
		return itemArray[index];
	}//end getItem
	
	public boolean isFull()
	{
		return numItems == ORDER - 1;
	}//end isFull
	
	public int findItem(long key)
	{
		for(int j = 0; j < numItems; j++)
			if(itemArray[j].dData == key)
				return j;
		
		return -1;
	}//end findItem
	
		// insert new item into this node
	public int insertItem(DataItem newItem)
	{
			// (assumes node is not full)
		
		long newKey = newItem.dData;
		
		int j;
		
		for(j = 0; j < numItems; j++)
			if(itemArray[j].dData > newKey)
				break;
		
		for(int k = numItems; k > j; k--)
			itemArray[k] = itemArray[k - 1];
		
		itemArray[j] = newItem;
		numItems++;
		
		return j;
	}//end insertItem()
	
		// remove largestItem
	public DataItem removeItem()
	{
			// assumes node not empty
		DataItem temp = itemArray[numItems - 1];		// save item
		itemArray[numItems - 1] = null;
		numItems--;
		return temp;
	}
	
		// format "/24/56/74/"
	public void displayNode()
	{
		for(int j = 0; j < numItems; j++)
			System.out.print("/" + itemArray[j].dData);
		System.out.println("/");						// final /
	}
}//end class Node