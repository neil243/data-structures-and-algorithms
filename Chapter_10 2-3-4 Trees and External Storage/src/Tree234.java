//**********************************************************
// Author: Neil Kasanda
//
// Program: demonstrates a 234 tree
// Listing 10.1
//**********************************************************

public class Tree234
{
		// instance variable
	private Node root;
	
		// constructor
	public Tree234()
	{
		root = new Node();							// make root node
	}
	
	public int find(long key)
	{
		Node curNode = root;
		int dataItemIndex;
		
		while(true)
		{
			dataItemIndex = curNode.findItem(key);
			
			if(dataItemIndex != -1)
				return dataItemIndex;
			else if(curNode.isLeaf())
				return -1;
			else
				curNode = getNextChild(curNode, key);
		}//end while
	}
	
		// insert a data item
	public void insert(long dValue)
	{
		Node curNode = root;
		DataItem newItem = new DataItem(dValue);
		
		while(true)
		{
			if(curNode.isFull())							// if node full,
			{
				split(curNode);								// split it
				curNode = curNode.getParent();				// back up
				
				curNode = getNextChild(curNode, dValue);
			}// end if(node is full)
			
			else if(curNode.isLeaf())						// if node is leaf,
				break;										// go insert
			
				// node is not full and is not leaf; go to lower level
			else curNode = getNextChild(curNode, dValue);
		}//end while
		
		curNode.insertItem(newItem);						// insert new DataItem
	}// end insert
	
	public void split(Node thisNode)
	{
			// assumes node is full
		DataItem itemB, itemC;
		Node parent, child2, child3;
		int itemIndex;
		
			// remove items from this node
		itemC = thisNode.removeItem();
		itemB = thisNode.removeItem();
		
			// remove children from this node
		child2 = thisNode.disconnectChild(2);
		child3 = thisNode.disconnectChild(3);
		
		Node newRight = new Node();							// Make new node
		
			// if this node is the root,
		if(thisNode == root)
		{
			root = new Node();					// make new root
			parent = root;						// new root is new parent
			root.connectChild(0, thisNode);		// connect to parent
		}
			// this node is not the root
		else
			parent = thisNode.getParent();
		
			// deal with parent
		itemIndex = parent.insertItem(itemB);	// itemB to parent
		int n = parent.getNumItems();			// total number of items?
		
			// move parent's connections one child to the right
		for(int j = n - 1; j > itemIndex; j--)
		{
			Node temp = parent.disconnectChild(j);
			parent.connectChild(j + 1, temp);
		}
		
			// connect newRight to parent
		parent.connectChild(itemIndex + 1, newRight); //???????????????????????????????????
		
			// deal with newRight
		newRight.insertItem(itemC);				// itemC to newRight
		newRight.connectChild(0, child2);		// connect child 2 and 3 from split node
		newRight.connectChild(1, child3);		// to child 0 and 1 on newRight
	}// end split()
	
		// gets appropriate child of node during search for value
	public Node getNextChild(Node theNode, long theValue)
	{
		int j;
			// assumes node is not empty, not full, not a leaf
		int numItems = theNode.getNumItems();
		
		for(j = 0; j < numItems; j++)			// for each item in node
		{										// are we less?
			if(theValue < theNode.getItem(j).dData)
				return theNode.getChild(j);		// return left child
		}// end for
		
			// we're greater, so return rightmost child 
			//   (won't be last child since node is not full)
		return theNode.getChild(j);
	}
	
	public void displayTree()
	{
		recDisplayTree(root, 0, 0);
	}
	
	public void recDisplayTree(Node thisNode, int level, int childNumber)
	{
		System.out.print("level = " + level + ", child = " + childNumber + " ");
		thisNode.displayNode();
		
			// call ourselves for each child of this node
		int numItems = thisNode.getNumItems();
		for(int j = 0; j < numItems + 1; j++)
		{
			Node nextNode = thisNode.getChild(j);
			if(nextNode != null)
				recDisplayTree(nextNode, level + 1, j);
			else
				return;
		}
		System.out.println();
	}//end recDisplayTree()
}