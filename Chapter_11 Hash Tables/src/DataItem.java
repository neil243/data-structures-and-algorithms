//***************************************************************
// Author: Neil Kasanda
//
// Program: data item type to be used in hash table
// Listing 11.1
//***************************************************************

public class DataItem
{
		// instance variables
	private int iData;	//(data item key)
	
		// constructor
	public DataItem(int ii)
	{
		iData = ii;
	}
	
		// return key of this data item
	public int getKey()
	{
		return iData;
	}
}//end class DataItem