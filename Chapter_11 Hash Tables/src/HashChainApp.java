//*************************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class HashTableSC
// Listing 11.3
//*************************************************************

import java.util.*;

public class HashChainApp
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int aKey;
		Link aLinkItem;
		int size, n, keysPerCell = 100;
		char choice;
		
		System.out.print("Enter size of hash table: ");
		size = console.nextInt();
		System.out.println();
		
		System.out.print("Enter initial number of items: ");
		n = console.nextInt();
		System.out.println();
		
			// make table
		HashTableSC theHashTable = new HashTableSC(size);
		for(int j = 0; j < n; j++)
		{
			aKey = (int)(Math.random() * keysPerCell * size);
			aLinkItem = new Link(aKey);
			theHashTable.insert(aLinkItem);
		}
		
		while(true)
		{
			System.out.print("Enter first letter of ");
			System.out.print("show, insert, delete, or find: ");
			choice = console.next().charAt(0);
			
			switch(choice)
			{
			case 's':
				theHashTable.displayTable();
				break;
				
			case 'i':
				System.out.print("Enter key value to insert: ");
				aKey = console.nextInt();
				aLinkItem = new Link(aKey);
				theHashTable.insert(aLinkItem);
				break;
				
			case 'd':
				System.out.print("Enter key value to delete: ");
				aKey = console.nextInt();
				theHashTable.delete(aKey);
				break;
				
			case 'f':
				System.out.print("Enter key value to find: ");
				aKey = console.nextInt();
				aLinkItem = theHashTable.find(aKey);
				if(aLinkItem != null)
					System.out.println("Found " + aKey);
				else
					System.out.println("Could not find " + aKey);
				break;
				
			default:
				System.out.println("Invalid entry");
			}//end switch
		}//end while
	}//end main()
}//end class HashChainApp