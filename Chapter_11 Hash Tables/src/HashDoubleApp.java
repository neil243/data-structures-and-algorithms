//***************************************************************
// Author: Neil Kasanda
//
// Program: implementation of a hash table of DataItems
// This hash table uses double hashing to handle collisions
// Listing 11.2
//***************************************************************

import java.util.*;

public class HashDoubleApp
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int aKey;
		DataItem aDataItem;
		int size, n;
		char choice;
		
			// get sizes
		System.out.print("Enter size of hash table: ");
		size = console.nextInt();
		
		System.out.print("Enter initial number of items: ");
		n = console.nextInt();
		
			// make table
		HashTableDH theHashTable = new HashTableDH(size);
			// insert data
		for(int j = 0; j < n; j++)
		{
			aKey = (int)(Math.random() * size);
			aDataItem = new DataItem(aKey);
			theHashTable.insert(aKey, aDataItem);
		}
		
			// interact with user
		while(true)
		{
			System.out.print("Enter the first letter of ");
			System.out.print("show, insert, delete, or find: ");
			choice = console.next().charAt(0);
			
			switch(choice)
			{
			case 's':
				theHashTable.displayTable();
				break;
				
			case 'i':
				System.out.print("Enter key value to insert: ");
				aKey = console.nextInt();
				aDataItem = new DataItem(aKey);
				theHashTable.insert(aKey, aDataItem);
				break;
				
			case 'd':
				System.out.print("Enter key value to delete: ");
				aKey = console.nextInt();
				theHashTable.delete(aKey);
				break;
				
			case 'f':
				System.out.print("Enter key value to find: ");
				aKey = console.nextInt();
				aDataItem = theHashTable.find(aKey);
				if(aDataItem != null)
					System.out.println("Found " + aKey);
				else
					System.out.println("Could not find " + aKey);
				break;
				
			default:
				System.out.println("Invalid entry");
			}//end switch()
		}//end while
	}//end main()
}