//***************************************************************
// Author: Neil Kasanda
//
// Program: implementation of a hash table of DataItems
// This hash table uses linear probing to solve collisions
// Listing 11.1
//***************************************************************

public class HashTable
{
		// instance variables
	private DataItem[] hashArray;				// array holds has values
	private int arraySize;
	private DataItem nonItem;					// for deleted items
	
		// constructor
	public HashTable(int size)
	{
		arraySize = size;
		hashArray = new DataItem[arraySize];
		nonItem = new DataItem(-1);				// deleted item key is -1
	}
	
	public void displayTable()
	{
		System.out.print("Table: ");
		for(int j = 0; j < arraySize; j++)
		{
			if(hashArray[j] != null)
				System.out.print(hashArray[j].getKey() + " ");
			else
				System.out.print("** ");
		}
		System.out.println();
	}
	
		// hash function
	public int hashFunc(int key)
	{
		return key % arraySize;
	}
	
		// insert a DataItem
	public void insert(DataItem item)
	{
		int key = item.getKey();
		int hashVal = hashFunc(key);
		
		while(hashArray[hashVal] != null && hashArray[hashVal].getKey() != -1)
		{
			hashVal = (hashVal + 1) % arraySize;		// go to next cell and 
		}												// wrap around if necessary
		
		hashArray[hashVal] = item;	// insert item
	}// end insert
	
		// delete a data item
	public DataItem delete(int key)
	{
		int hashVal = hashFunc(key);		// hash the key
		
		while(hashArray[hashVal] != null && hashArray[hashVal].getKey() != -1)
		{
			if(hashArray[hashVal].getKey() == key)
			{
				DataItem temp = hashArray[hashVal];
				hashArray[hashVal] = nonItem;
				return temp;
			}
			hashVal = (hashVal + 1) % arraySize;
		}
		return null;	// can't find item
	}//end delete()
	
		// find item with key
	public DataItem find(int key)
	{
		int hashVal = hashFunc(key);
		
		while(hashArray[hashVal] != null && hashArray[hashVal].getKey() != -1)
		{
			if(hashArray[hashVal].getKey() == key)	// found it?
				return hashArray[hashVal];			// yes, return it
			
			hashVal = (hashVal + 1) % arraySize;	// no, go to next cell
		}											// and wrap around if necessary
		
		return null;	// can't find item
	}
}//end class HashTable