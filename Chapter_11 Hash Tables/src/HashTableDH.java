//***************************************************************
// Author: Neil Kasanda
//
// Program: implementation of a hash table of DataItems
// This hash table uses double hashing to handle collisions
// Listing 11.2
//***************************************************************

public class HashTableDH
{
		// instance variables
	private DataItem[] hashArray;				// array is the hash table
	private int arraySize;
	private DataItem nonItem;					// for deleted items
	
		// constructor
	public HashTableDH(int size)
	{
		arraySize = size;
		hashArray = new DataItem[arraySize];
		nonItem = new DataItem(-1);
	}
	
	public void displayTable()
	{
		System.out.print("Table: ");
		for(int j = 0; j < arraySize; j++)
		{
			if(hashArray[j] != null)
				System.out.print(hashArray[j] + " ");
			else
				System.out.print("** ");
		}
		System.out.println();
	}
	
	public int hashFunc1(int key)
	{
		return key % arraySize;
	}
	
	public int hashFunc2(int key)
	{
			// non-zero, less than array size, different from hF1
			// array size must be relatively prime to 5, 4, 3, and 2
		return 5 - key % 5;
	}
	
	public void insert(int key, DataItem item)
	{
		int hashVal = hashFunc1(key);	// hash the key
		int stepSize = hashFunc2(key);	// get step size
		
			// until empty cell or -1 (deleted cell)
		while(hashArray[hashVal] != null && hashArray[hashVal].getKey() != -1)
		{
			hashVal = (hashVal + stepSize) % arraySize; 	// step, and if necessary wrap
		}													// around
		
		hashArray[hashVal] = item;							// insert item
	}//end insert()
	
		// delete a DataItem
	public DataItem delete(int key)
	{
		int hashVal = hashFunc1(key);
		int stepSize = hashFunc2(key);
		
			// until empty cell
		while(hashArray[hashVal] != null)
		{
			if(hashArray[hashVal].getKey() == key)
			{
				DataItem temp = hashArray[hashVal];
				hashArray[hashVal] = nonItem;
				return temp;
			}
			hashVal = (hashVal + stepSize) % arraySize;		// step, and if necessary wrap
		}													// around
		return null;
	}//end delete
	
		// find item with key
		// (assumes table not full, or this method will loop forever
		// 			if it can't find the item)
	public DataItem find(int key)
	{
		int hashVal = hashFunc1(key);
		int stepSize = hashFunc2(key);
		
			// until empty cell
		while(hashArray[hashVal] != null)
		{
			if(hashArray[hashVal].getKey() == key)
				return hashArray[hashVal];
			
			hashVal = (hashVal + stepSize) % stepSize;
		}
		return null;
	}//end find
	
}//end class HashTableDH