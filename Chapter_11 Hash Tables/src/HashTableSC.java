//*************************************************************
// Author: Neil Kasanda
//
// Program: implements a hash table with separate chaining (SC)
// Listing 11.3
//*************************************************************

public class HashTableSC
{
		// instance variables
	private SortedList[] hashArray;					// array of lists
	private int arraySize;
	
		// constructor
	public HashTableSC(int size)
	{
		arraySize = size;
		hashArray = new SortedList[arraySize];		// create array
		for(int j = 0; j < arraySize; j++)			// fill array
			hashArray[j] = new SortedList();		// with lists
	}
	
		// display table
	public void displayTable()
	{
		for(int j = 0; j < arraySize; j++)
		{
			System.out.print(j + ". ");				// display cell number
			hashArray[j].displayList();				// display list
		}
	}
	
		// hash function
	public int hashFunc(int key)
	{
		return key % arraySize;
	}
	
	public void insert(Link theLink)
	{
		int key = theLink.getKey();
		int hashVal = hashFunc(key);				// hash the key
		hashArray[hashVal].insert(theLink);			// insert at hashVal
	}//end insert
	
	public Link find(int key)
	{
		int hashVal = hashFunc(key);				// hash the key
		Link theLink = hashArray[hashVal].find(key);// get link
		return theLink;								// return link
	}
	
	public void delete(int key)						// delete a link
	{
		int hashVal = hashFunc(key);				// hash the key
		hashArray[hashVal].delete(key);				// delete link
	}//end delete
}//end HashTableSC