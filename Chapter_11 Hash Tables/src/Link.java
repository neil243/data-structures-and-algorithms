//*******************************************************
// Author: Neil Kasanda
//
// Program: implements a link of a sorted linked-list
// Listing 11.3
//*******************************************************

public class Link
{
		// instance variables
	private int iData;						// data item
	public Link next;						// next link in list
	
		// constructor
	public Link(int it)
	{
		iData = it;
	}
	
	public int getKey()
	{
		return iData;
	}
	
		// display this link
	public void displayLink()
	{
		System.out.print(iData + " ");
	}
}//end class Link