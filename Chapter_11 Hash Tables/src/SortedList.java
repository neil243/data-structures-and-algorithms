//*******************************************************
// Author: Neil Kasanda
//
// Program: implements a sorted linked-list
// Listing 11.3
//*******************************************************

public class SortedList
{
	private Link first;						// ref to first list item
	
		// constructor
	public SortedList()
	{
		first = null;
	}
	
		// insert link in order
	public void insert(Link theLink)
	{
		int key = theLink.getKey();
		Link previous = null;
		Link current = first;
		
		while(current != null && key > current.getKey())
		{
			previous = current;
			current = current.next;
		}
		
		theLink.next = current;
		
			// if beginning of list
		if(previous == null)
			first = theLink;
		else
			previous.next = theLink;
	}//end insert()
	
		// delete link (assumes non-empty list)
	public void delete(int key)
	{
		Link previous = null;
		Link current = first;
		
		while(current != null && current.getKey() != key)
		{
			previous = current;
			current = current.next;
		}
		
		if(previous == null)				// if beginning of list,
			first = first.next;				//		delete first link
		else								// not beginning,
			previous.next = current.next;	// 		delete current link
	}//end delete()
	
	public Link find(int key)				// find link
	{
		Link current = first;				// start at first
		
		while(current != null && current.getKey() <= key)
		{
			if(current.getKey() == key)
				return current;
			
			current = current.next;
		}
		
		return null;						// didn't find it
	}
	
		// display this list
	public void displayList()
	{
		System.out.print("List (first-->last): ");
		Link current = first;
		while(current != null)
		{
			current.displayLink();
			current = current.next;
		}
		System.out.println();
	}
}//end SortedList