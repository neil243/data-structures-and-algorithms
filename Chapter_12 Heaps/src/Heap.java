//****************************
// Author: Neil Kasanda
//
// Program: implements a heap
// Listing 12.1
//****************************

public class Heap
{
		// instance variables
	private Node[] heapArray;
	private int maxSize;							// size of array
	private int currentSize;						// number of nodes in array
	
		// constructor
	public Heap(int max)
	{
		maxSize = max;
		heapArray = new Node[maxSize];				// create array
		currentSize = 0;
	}
	
	public boolean isEmpty()
	{
		return currentSize == 0;
	}
	
	public boolean insert(int key)
	{
		if(currentSize == maxSize)
			return false;
		
		Node newNode = new Node(key);
		heapArray[currentSize] = newNode;
		trickleUp(currentSize++);		
		return true;
	}//end insert
	
	public void trickleUp(int index)
	{
		int parent = (index - 1) / 2;
		Node bottom = heapArray[index];
		
		while(index > 0 && heapArray[parent].getKey() < bottom.getKey())
		{
			heapArray[index] = heapArray[parent];
			index = parent;
			parent = (parent - 1) / 2;
		}//end while
		heapArray[index] = bottom;
	}//end trickleUp()
	
	public Node remove()
	{
		Node root = heapArray[0];
		heapArray[0] = heapArray[--currentSize];
		trickleDown(0);
		return root;
	}//end remove()
	
	public void trickleDown(int index)
	{
		Node top = heapArray[index];			// save root
		int largerChild;
		
		while(index < currentSize / 2)
		{
			int leftChild = 2 * index + 1;
			int rightChild = leftChild + 1;
			
			if(rightChild < currentSize && 
					heapArray[leftChild].getKey() < heapArray[rightChild].getKey())
				largerChild = rightChild;
			
			else largerChild = leftChild;
			
			if(top.getKey() >= heapArray[largerChild].getKey())
				break;
			
			heapArray[index] = heapArray[largerChild];
			index = largerChild;
		}
		heapArray[index] = top;
	}//end trickleDown
	
	public boolean change(int index, int newValue)
	{
		if(index < 0 || index >= currentSize)
			return false;
		
		int oldValue = heapArray[index].getKey();
		heapArray[index].setKey(newValue);
		
		if(oldValue < newValue)
			trickleUp(index);
		else
			trickleDown(index);
		return true;
	}//end change
	
	public void displayHeap()
	{
			// array format
		System.out.print("heapArray: ");
		for(int i = 0; i < currentSize; i++)
			System.out.print(heapArray[i].getKey() + " ");
		System.out.println();
		
			// heap format
		int nBlanks = 32;
		int itemsPerRow = 1;
		int column = 0;
		int j = 0;									// current item
		String dots = ".......................................";
		System.out.println(dots + dots);			// dotted top line
		
		while(currentSize > 0)
		{
			if(column == 0)							// first item in row?
				for(int k = 0; k < nBlanks; k++)	// preceding blacks
					System.out.print(" ");
			
			System.out.print(heapArray[j].getKey());	// display item
			
			if(++j == currentSize)						// done?
				break;
			
			if(++column == itemsPerRow)					// end of row?
			{
				nBlanks = nBlanks / 2;
				itemsPerRow = itemsPerRow * 2;			// twice the items
				column = 0;								// start over on new row
				System.out.println();
			}
			else										// next item on row
			{
				for(int k = 0; k < nBlanks * 2 - 2; k++)
					System.out.print(" ");				// interim blanks
			}
		}//end while
		System.out.println("\n" + dots + dots);		// dotted bottom line
	}//end displayHeap()
	
}//end class Heap