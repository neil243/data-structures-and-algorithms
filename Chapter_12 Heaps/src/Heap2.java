//***************************************************
// Author: Neil Kasanda
//
// Program: demonstrates heap sort
// Listing 12.2
//***************************************************

public class Heap2
{
		// instance variables
	private Node[] heapArray;
	private int maxSize;
	private int currentSize;
	
		// constructor
	public Heap2(int max)
	{
		maxSize = max;
		currentSize = 0;
		heapArray = new Node[maxSize];
	}
	
		// delete item with max key
		// (assumes non-empty heap)
	public Node remove()
	{
		Node root = heapArray[0];
		heapArray[0] = heapArray[--currentSize];
		trickleDown(0);
		return root;
	}
	
	public void trickleDown(int index)
	{
		Node top = heapArray[index];
		int largerChild;
		
		while(index < currentSize / 2)
		{
			int leftChild = index * 2 + 1;
			int rightChild = leftChild + 1;
			
			if(rightChild < currentSize && 
					heapArray[leftChild].getKey() < heapArray[rightChild].getKey())
				largerChild = rightChild;
			else
				largerChild = leftChild;
			
			if(top.getKey() >= heapArray[largerChild].getKey())
				break;
			
			heapArray[index] = heapArray[largerChild];
			index = largerChild;
		}
		heapArray[index] = top;
	}//end trickleDown
	
	public void displayHeap()
	{
		int nBlanks = 32;
		int itemsPerRow = 1;
		int column = 0;
		int j = 0;										// current item
		String dots = "...........................................";
		
		System.out.println(dots + dots);				// dotted top line
		
		while(currentSize > 0)
		{
			if(column == 0)
				for(int k = 0; k < nBlanks; k++)
					System.out.print(" ");
			
				// display item
			System.out.print(heapArray[j].getKey());
			
			if(++j == currentSize)
				break;
			
			if(++column == itemsPerRow)					// end of row?
			{
				nBlanks = nBlanks / 2;
				itemsPerRow = itemsPerRow * 2;
				column = 0;
				System.out.println();
			}
			
			else
				for(int k = 0; k < nBlanks * 2 - 2; k++)
					System.out.print(" ");				// interim blanks				
		}//end while
		System.out.println("\n" + dots + dots);			// dotted bottom line
	}//end displayHeap()
	
	public void displayArray()
	{
		for(int j = 0; j < maxSize; j++)
			System.out.print(heapArray[j].getKey() + " ");
		System.out.println();
	}
	
	public void insertAt(int index, Node newNode)
	{
		heapArray[index] = newNode;
	}
	
	public void incrementSize()
	{
		currentSize++;
	}	
}//end class Heap2