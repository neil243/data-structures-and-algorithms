//*********************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the Heap class
// Listing 12.1
//*********************************************************

import java.util.*;

public class HeapApp
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int value, value2;
		Heap theHeap = new Heap(31);		// make a heap with maxSize of 31
		boolean success;
		int choice;
		
			// insert 10 items
		theHeap.insert(70);
		theHeap.insert(40);
		theHeap.insert(50);
		theHeap.insert(20);
		theHeap.insert(60);
		theHeap.insert(100);
		theHeap.insert(80);
		theHeap.insert(30);
		theHeap.insert(10);
		theHeap.insert(90);
		
		System.out.print("Enter first letter of ");
		System.out.print("show, insert, remove, change, or quit: ");
		choice = console.next().charAt(0);
		
		while(choice != 'q')
		{			
			switch(choice)
			{
			case 's':
				theHeap.displayHeap();
				break;
				
			case 'i':
				System.out.print("Enter value to insert: ");
				value = console.nextInt();
				success = theHeap.insert(value);
				if(!success)
					System.out.println("Can't insert; heap full");
				break;
				
			case 'r':
				if(!theHeap.isEmpty())
					theHeap.remove();
				else
					System.out.println("Can't remove; heap empty");
				break;
				
			case 'c':
				System.out.print("Enter current index of item: ");
				value = console.nextInt();
				System.out.print("Enter new key: ");
				value2 = console.nextInt();
				success = theHeap.change(value, value2);
				if(!success)
					System.out.println("Invalid index");
				break;
				
			default:
				System.out.println("Invalid entry");
			}//end switch
			
			System.out.print("Enter first letter of ");
			System.out.print("show, insert, remove, change, or quit: ");
			choice = console.next().charAt(0);
		}//end while
		
		console.close();
	}//end main()
}
