//*****************************************************************
// Author: Neil Kasanda
//
// Program: to test the operations of the class Heap2 (heap sort)
// Listing 12.2
//*****************************************************************

import java.util.*;

public class HeapSortApp
{
	private static Scanner console = new Scanner(System.in);
	
	public static void main(String[] args)
	{
		int size, j;
		
		System.out.print("Enter number of items: ");
		size = console.nextInt();
		Heap2 theHeap = new Heap2(size);
		
			// fill array with random nodes
		for(j = 0; j < size; j++)
		{
			int random = (int)(Math.random() * 100);
			Node newNode = new Node(random);
			theHeap.insertAt(j, newNode);
			theHeap.incrementSize();
		}
		
			// display random array
		System.out.print("Random: ");
		theHeap.displayArray();
		
			// make random array into heap
		for(j = size / 2 - 1; j >= 0; j--)
			theHeap.trickleDown(j);
		
		System.out.print("Heap:   ");
		theHeap.displayArray();					// display heap array
		theHeap.displayHeap();					// display heap
		
		for(j = size - 1; j >= 0; j--)
		{
			Node biggestNode = theHeap.remove();
			theHeap.insertAt(j, biggestNode);
		}
		
			// display sorted array
		System.out.print("Sorted: ");
		theHeap.displayArray();					// display array
	}//end main()
}//end class HeapSortApp