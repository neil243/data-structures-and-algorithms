//*********************************************************
// Author: Neil Kasanda
//
// Program: implements a node of a heap
// Listing 12.1
//*********************************************************

public class Node
{
		// instance variables
	private int iData;							// data item (key)
	
		// constructor
	public Node(int key)
	{
		iData = key;
	}
	
	public int getKey()
	{
		return iData;
	}
	
	public void setKey(int key)
	{
		iData = key;
	}
}//end class Node