//************************************************************
// Author: Neil Kasanda
//
// Program: implements a graph algorithm
// Listing 13.1
//************************************************************

public class Graph
{
	private final int MAX_VERTS = 20;
	private Vertex[] vertexList;					// list of vertices
	private int[][] adjMat;							// adjacency matrix
	private int nVerts;								// current number of vertices
	private StackX theStack;
	
	public Graph()
	{
		vertexList = new Vertex[MAX_VERTS];
		adjMat = new int[MAX_VERTS][MAX_VERTS];		// arrays are initialized to default vals
		nVerts = 0;
		theStack = new StackX();
	}//end constructor
	
	public void addVertex(char lab)
	{
		vertexList[nVerts++] = new Vertex(lab);
	}
	
	public void addEdge(int start, int end)
	{
		adjMat[start][end] = 1;
		adjMat[end][start] = 1;
	}
	
	public void displayVertex(int v)
	{
		System.out.print(vertexList[v].label);
	}
	
		// depth-first search
	public void dfs()
	{
			// begin at vertex 0
		vertexList[0].wasVisited = true;		// mark it
		displayVertex(0);						// display it
		theStack.push(0);						// push it
		
		while(!theStack.isEmpty())
		{
				// get an unvisited vertex adjacent to stack top
			int v = getAdjUnvisitedVertex(theStack.peek());
			if(v == -1)
				theStack.pop();
			else
			{
				vertexList[v].wasVisited = true;
				displayVertex(v);
				theStack.push(v);
			}
		}//end while
		
			// stack is empty, so we're done
		for(int j = 0; j < nVerts; j++)
			vertexList[j].wasVisited = false;		// reset flags
	}//end dfs
	
		// returns an unvisited vertex adj to v
	public int getAdjUnvisitedVertex(int v)
	{
		for(int j = 0; j < nVerts; j++)
			if(adjMat[v][j] == 1 && vertexList[j].wasVisited == false)
				return j;
		return -1;
	}//end getAdjUnvisitedVertex()
}//end class Graph