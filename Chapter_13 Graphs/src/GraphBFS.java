//******************************************************
// Author: Neil Kasanda
//
// Program: demonstrates breath-first search algorithm
// Listing 13.2
//******************************************************

public class GraphBFS
{
		// instance variables
	private final int MAX_VERTS = 20;
	private Vertex[] vertexList;			// list of vertices
	private int[][] adjMat;					// adjacency matrix
	private int nVerts;						// current number of vertices
	private Queue theQueue;
	
		// constructor
	public GraphBFS()
	{
		vertexList = new Vertex[MAX_VERTS];
		adjMat = new int[MAX_VERTS][MAX_VERTS];
		nVerts = 0;
		theQueue = new Queue();
	}
	
	public void addVertex(char lab)
	{
		vertexList[nVerts++] = new Vertex(lab);
	}
	
	public void addEdge(int start, int end)
	{
		adjMat[start][end] = 1;
		adjMat[end][start] = 1;
	}
	
	public void displayVertex(int v)
	{
		System.out.print(vertexList[v].label);
	}
	
		// breath-first search
	public void bfs()
	{
			// begin at vertex 0
		vertexList[0].wasVisited = true;
		displayVertex(0);
		theQueue.insert(0);
		int v2;
		
			// until queue is empty
		while(!theQueue.isEmpty())
		{
			int v1 = theQueue.remove();				// remove vertex at head
				// until it has no unvisited neighbors
			while((v2 = getAdjUnvisitedVertex(v1)) != -1)
			{
				vertexList[v2].wasVisited = true;
				displayVertex(v2);
				theQueue.insert(v2);
			}//end while
		}//end while (queue not empty)
		
			// queue is empty, so we're done
		for(int j = 0; j < nVerts; j++)
			vertexList[j].wasVisited = false;
	}//end bfs()
		
		// returns an unvisited vertex adj to v
	public int getAdjUnvisitedVertex(int v)
	{
		for(int j = 0; j < nVerts; j++)
			if(adjMat[v][j] == 1 && vertexList[j].wasVisited == false)
				return j;
		return -1;
	}//end getAdjUnvisitedVertex()
}//end class Graph