//**********************************************************
// Author: Neil Kasanda
//
// Program: implements a graph with a minimum spanning tree
// method
// Listing 13.3
//**********************************************************

public class GraphMST
{
		// instance variables
	private final int MAX_VERTS = 20;
	private Vertex[] vertexList;		// list of vertices
	private int[][] adjMat;				// adjacency matrix
	private int nVerts;					// current number of vertices
	private StackX theStack;
	
		// constructor
	public GraphMST()
	{
		vertexList = new Vertex[MAX_VERTS];
		adjMat = new int[MAX_VERTS][MAX_VERTS];
		nVerts = 0;
		theStack = new StackX();
	}
	
	public void addVertex(char lab)
	{
		vertexList[nVerts++] = new Vertex(lab);
	}
	
	public void addEdge(int start, int end)
	{
		adjMat[start][end] = 1;
		adjMat[end][start] = 1;
	}
	
	public void displayVertex(int v)
	{
		System.out.print(vertexList[v].label);
	}
	
		// minimum spanning tree (depth-first)
	public void mst()
	{
			// start at 0
		vertexList[0].wasVisited = true;		// mark it
		theStack.push(0);						// push it
		
		while(!theStack.isEmpty())
		{
			int currentVertex = theStack.peek();
			
				//get next unvisited neighbor
			int v = getAdjUnvisitedVertex(currentVertex);
			if(v == -1)
				theStack.pop();
			else
			{
				vertexList[v].wasVisited = true;		// mark it
				theStack.push(v);						// push it
				
					// display edge
				displayVertex(currentVertex);
				displayVertex(v);
				System.out.print(" ");
			}
		}//end while (stack not empty)
		
			// stack is empty, so we're done
		for(int j = 0; j < nVerts; j++)
			vertexList[j].wasVisited = false;			// reset flags
	}//end mst
	
		// returns an unvisited vertex adj to v
	public int getAdjUnvisitedVertex(int v)
	{
		for(int j = 0; j < nVerts; j++)
			if(adjMat[v][j] == 1 && vertexList[j].wasVisited == false)
				return j;
		return -1;
	}
}//end class GraphMST