//*******************************************************
// Author: Neil Kasanda
//
// Program: demonstrates topological sorting
// Listing 13.3
//*******************************************************

public class GraphTopo
{
		// instance variables
	private final int MAX_VERTS = 20;
	private Vertex[] vertexList;
	private int[][] adjMat;
	private int nVerts;
	private char[] sortedArray;
	
		// constructor
	public GraphTopo()
	{
		vertexList = new Vertex[MAX_VERTS];
		adjMat = new int[MAX_VERTS][MAX_VERTS];
		nVerts = 0;
		sortedArray = new char[MAX_VERTS];
	}
	
	public void addVertex(char lab)
	{
		vertexList[nVerts++] = new Vertex(lab);
	}
	
	public void addEdge(int start, int end)
	{
		adjMat[start][end] = 1;
	}
	
	public void displayVertex(int v)
	{
		System.out.print(vertexList[v].label);
	}
	
		// topological sort
	public void topo()
	{
		int orig_nVerts = nVerts; 						// remember how many verts
		
			// while vertices remain,
		while(nVerts > 0)
		{
				// get a vertex with no successor, or -1
			int currentVertex = noSuccessor();
			
				// for debugging purposes
			System.out.println("Successor is: " + vertexList[currentVertex].label);
			
			if(currentVertex == -1)	// there must be a cycle
			{
				System.out.println("E R R O R: Graph has cycles");
				return;
			}
			
				// insert vertex label in sorted array (start at end)
			sortedArray[nVerts - 1] = vertexList[currentVertex].label;
			
			deleteVertex(currentVertex);				// delete vertex
		}//end while
		
			// vertices all gone; display sortedArray
		System.out.print("Topologically sorted order: ");
		for(int j = 0; j < orig_nVerts; j++)
			System.out.print(sortedArray[j]);
		System.out.println();
	}//end topo
	
		// returns vertex with no successors
		// (or -1 if no such verts)
	public int noSuccessor()
	{
		boolean isEdge;			// edge from row to column in adjMat
		
		for(int row = 0; row < nVerts; row++)
		{
			isEdge = false;
			
			for(int col = 0; col < nVerts; col++)
			{
				if(adjMat[row][col] > 0)			// this vector has a successor
				{
					isEdge = true;					// since it has an edge to another vector
					break;
				}
			}
			if(!isEdge)			// if no edges, has no successors
				return row;
		}
		return -1;				// no such vertex
	}//end noSuccessors()
	
	public void deleteVertex(int delVert)
	{
		if(delVert != nVerts - 1)	// if no last vertex
		{
				// delete from vertexList
			for(int j = delVert; j < nVerts - 1; j++)
				vertexList[j] = vertexList[j + 1];
			
				// delete row from adjMat
			for(int row = delVert; row < nVerts - 1; row++)
				moveRowUp(row, nVerts);
			
				// delete col from adjMat
			for(int col = delVert; col < nVerts - 1; col++)
				moveColLeft(col, nVerts - 1);	// because we've already deleted
												// one row
		}
		nVerts--;	// one less vertex
	}//end deleteVertex
	
	private void moveRowUp(int row, int length)
	{
		for(int col = 0; col < length; col++)
			adjMat[row][col] = adjMat[row + 1][col];
	}
	
	private void moveColLeft(int col, int length)
	{
		for(int row = 0; row < length; row++)
			adjMat[row][col] = adjMat[row][col + 1];
	}
}//end class GraphTopo