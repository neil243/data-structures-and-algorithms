//******************************************************
// Author: Neil Kasanda
//
// Program: implements a queue for use in breath-first
// search graph algorithm
// Listing 13.2
//******************************************************

public class Queue 
{
		// instance variables
	private final int SIZE = 20;
	private int[] queArray;
	private int front;
	private int rear;
	
		// constructor
	public Queue()
	{
		queArray = new int[SIZE];
		front = 0;
		rear = -1;
	}
	
		// put item at rear of queue
	public void insert(int j)
	{
		queArray[(++rear) % SIZE] = j;
	}
	
		// take item from front of queue
	public int remove()
	{
		int temp = queArray[front];
		front = (front + 1) % SIZE;
		return temp;
	}
	
		// true if queue is empty
	public boolean isEmpty()
	{
		return rear + 1 == front || front + SIZE - 1 == rear;
	}
}//end class Queue