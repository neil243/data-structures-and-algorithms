//************************************************************
// Author: Neil Kasanda
//
// Program: implements a stack for use in depth-first search
// graph algorithm
// Listing 13.1
//************************************************************

public class StackX
{
		// instance variables
	private final int SIZE = 20;
	private int[] stack;
	private int top;
	
		// constructor
	public StackX()
	{
		stack = new int[SIZE];			// make array
		top = -1;
	}
	
		// put item on stack
	public void push(int j)
	{
		stack[++top] = j;
	}
	
		// take item off stack
	public int pop()
	{
		return stack[top--];
	}
	
		// peek at top of stack
	public int peek()
	{
		return stack[top];
	}
	
		// true if nothing on stack
	public boolean isEmpty()
	{
		return top == -1;
	}
}//end class StackX