//************************************************************
// Author: Neil Kasanda
//
// Program: implements a vertex for use Graph class
// Listing 13.1
//************************************************************

public class Vertex
{
		// instance variable
	public char label;					// label (e.g. 'A')
	public boolean wasVisited;
	
		// constructor
	public Vertex(char lab)
	{
		label = lab;
		wasVisited = false;
	}
}//end class Vertex