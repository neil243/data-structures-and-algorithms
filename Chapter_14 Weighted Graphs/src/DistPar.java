//***********************************************
// Author: Neil Kasanda
//
// Program: implements distance between parent
// vertex and current vertex
// Listing 14.2
//***********************************************

public class DistPar
{
		// instance variables
	public int distance;
	public int parentVert;
	
		// constructor
	public DistPar(int d, int pv)
	{
		distance = d;
		parentVert = pv;
	}
}// end class DistPar