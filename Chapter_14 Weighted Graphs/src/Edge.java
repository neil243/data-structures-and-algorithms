//*******************************************************************
// Author: Neil Kasanda
//
// Program: implements an edge in a graph
// Listing 14.1
//*******************************************************************

public class Edge
{
		// instance variables
	public int srcVert;			// index of a vertex starting edge
	public int destVert;		// index of a vertex ending edge
	public int distance;		// distance from src to dest
	
		// constructor
	public Edge(int sv, int dv, int d)
	{
		srcVert = sv;
		destVert = dv;
		distance = d;
	}
}//end class Edge