//*******************************************************************
// Author: Neil Kasanda
//
// Program: implements a weighted graph
// Listing 14.1
//*******************************************************************

public class Graph
{
		// instance variables
	private final int MAX_VERTS = 20;
	private final int INFINITY = 1000000;
	private Vertex[] vertexList;			// list of vertices
	private int adjMat[][];					// adjacency matrix
	private int nVerts;						// current number of vertices
	private int currentVert;
	private PriorityQ thePQ;
	private int nTree;
	
		// constructor
	public Graph()
	{
		vertexList = new Vertex[MAX_VERTS];
		
		adjMat = new int[MAX_VERTS][MAX_VERTS];		
		for(int row = 0; row < MAX_VERTS; row++)
			for(int col = 0; col < MAX_VERTS; col++)
				adjMat[row][col] = INFINITY;
		
		nVerts = 0;
		thePQ = new PriorityQ();
	}
	
	public void addVertex(char lab)
	{
		vertexList[nVerts++] = new Vertex(lab);
	}
	
	public void addEdge(int start, int end, int weight)
	{
		adjMat[start][end] = weight;
		adjMat[end][start] = weight;
	}
	
	public void displayVertex(int v)
	{
		System.out.print(vertexList[v].label);
	}
	
		// minimum spannig tree
	public void mstw()
	{
		currentVert = 0;				// start at 0
		
		while(nTree < nVerts - 1)
		{
			vertexList[currentVert].isInTree = true;
			nTree++;
			
				// insert edges adjacent to currentVert into PQ
			for(int j = 0; j < nVerts; j++)
			{
				if(j == currentVert)
					continue;
				else if(vertexList[j].isInTree == true)
					continue;
				
				int distance = adjMat[currentVert][j];
				
				if(distance == INFINITY)
					continue;
				
				//System.out.println("Putting edge " + currentVert + "-" + j + ", distance " + distance);
				putInPQ(j, distance);
			}
			
			if(thePQ.size() == 0)
			{
				System.out.println(" GRAPH NOT CONNECTED");
				return;
			}
			
				// remove edge with minimum distance, from PQ
			Edge theEdge = thePQ.removeMin();
			int sourceVert = theEdge.srcVert;
			currentVert = theEdge.destVert;
			
				// display edge from source to current
			System.out.print(vertexList[sourceVert].label);
			System.out.print(vertexList[currentVert].label);
			System.out.print(" ");
		}//end while (not all verts in tree)
		
			// mst is complete
		for(int j = 0; j < nVerts; j++)
			vertexList[j].isInTree = false;
	}//end mstw()
	
	public void putInPQ(int newVert, int newDist)
	{
			// is there another edge with the same destination vertex?
		int queueIndex = thePQ.find(newVert);
		
		if(queueIndex != -1)
		{
			Edge tempEdge = thePQ.peekN(queueIndex);			// get edge
			int oldDist = tempEdge.distance;
			
			if(oldDist > newDist)
			{
				thePQ.removeN(queueIndex);						// remove old edge
				
				Edge theEdge = new Edge(currentVert, newVert, newDist);
				thePQ.insert(theEdge);							// insert new edge
			}
			//else no action; just leave the old vertex there
		}//end if
		
			// no edge with same destination vertex, so insert new one
		else
		{
			Edge theEdge = new Edge(currentVert, newVert, newDist);
			thePQ.insert(theEdge);
		}
	}//end putInPQ()
}