//***********************************************
// Author: Neil Kasanda
//
// Program: implements a weighted and directed
// graph with Dijkstra's shortest path algorithm
// Listing 14.2
//***********************************************

public class GraphSP
{
		// instance variables
	private final int MAX_VERTS = 20;
	private final int INFINITY = 1000000;
	private Vertex[] vertexList;				// list of vertices
	private int[][] adjMat;						// adjacency matrix
	private int nVerts;							// current number of vertices
	private int nTree;							// number of verts in tree
	private DistPar[] sPath;					// array of shortest-path data
	private int currentVert;					// current vertex
	private int startToCurrent;					// distance to currentVert
	
		// constructor
	public GraphSP()
	{
		vertexList = new Vertex[MAX_VERTS];
		
		adjMat = new int[MAX_VERTS][MAX_VERTS];
		for(int row = 0; row < MAX_VERTS; row++)
			for(int col = 0; col < MAX_VERTS; col++)
				adjMat[row][col] = INFINITY;
		
		nVerts = 0;
		nTree = 0;
		sPath = new DistPar[MAX_VERTS];
	}// end constructor
	
	public void addVertex(char lab)
	{
		vertexList[nVerts++] = new Vertex(lab);
	}
	
	public void addEdge(int start, int end, int weight)
	{
		adjMat[start][end] = weight;
	}
	
		// finds all shortest paths
	public void path()
	{
		int startTree = 0;
		vertexList[startTree].isInTree = true;
		nTree++;
		
			// transfer row of distances from adjMat to sPath
		for(int j = 0; j < nVerts; j++)
		{
			int tempDist = adjMat[startTree][j];
			sPath[j] = new DistPar(tempDist, startTree);
		}
		
			// until all vertices are in the tree
		while(nTree < nVerts)
		{
			int indexMin = getMin();					// get min from sPath
			int minDist = sPath[indexMin].distance;
			
			if(minDist == INFINITY)
			{
				System.out.println("There are unreachable vertices");
				break;
			}
			else
			{
				currentVert = indexMin;
				startToCurrent = sPath[indexMin].distance;
			}
			
				// put current vertex in tree
			vertexList[currentVert].isInTree = true;
			nTree++;
			adjust_sPath();	// update sPath array
		}//end while(nTree < nVerts)
		
			// display sPath[] contents
		displayPath();
		
			// clear tree
		nTree = 0;
		for(int j = 0; j < nVerts; j++)
			vertexList[j].isInTree = false;
	}//end path()
	
	public int getMin()
	{
		int minDist = INFINITY;
		int indexMin = 0;
		
		for(int j = 1; j < nVerts; j++)
		{
			if(!vertexList[j].isInTree && sPath[j].distance < minDist)
			{
				minDist = sPath[j].distance;
				indexMin = j;
			}
		}
		return indexMin;
	}//end getMin()
	
	public void adjust_sPath()
	{
			// adjust values in shortest-path array sPath
		int column = 1;			// skip starting vertex
		
		while(column < nVerts)	// go across all columns 
		{
			// if this column's vertex is already in tree, skip it
			if(vertexList[column].isInTree)
			{
				column++;
				continue;
			}
			
				// calculate distance for one sPath entry
					// get edge from current vertex to column
			int currentToFringe = adjMat[currentVert][column];
					// add distance from start
			int startToFringe = startToCurrent + currentToFringe;
					// get distance of current sPath entry
			int sPathDist = sPath[column].distance;
			
				// compare distance from start with sPath entry
			if(startToFringe < sPathDist)
			{
				sPath[column].parentVert = currentVert;
				sPath[column].distance = startToFringe;
			}
			column++;
		}//end while(column < nVerts)
	}//end adjust_sPath()
	
	public void displayPath()
	{
		for(int j = 0; j < nVerts; j++)
		{
			System.out.print(vertexList[j].label + "=");
			if(sPath[j].distance == INFINITY)
				System.out.print("inf");
			else
				System.out.print(sPath[j].distance);
			
			char parent = vertexList[ sPath[j].parentVert ].label;
			System.out.print("(" + parent + ") ");
		}
		System.out.println();
	}
}//end class Graph