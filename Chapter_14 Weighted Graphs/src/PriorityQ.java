//**********************************************************************
// Author: Neil Kasanda
//
// Program: implements a priority queue for use in the minimum spanning
// tree algorithm of a weighted graph
// Listing 14.1
//**********************************************************************

public class PriorityQ
{
	// array in sorted order, from max at 0 to min at size-1
	private final int SIZE = 20;
	private Edge[] queArray;
	private int size;
	
		// constructor
	public PriorityQ()
	{
		queArray = new Edge[SIZE];
		size = 0;
	}
	
		// insert item in sorted order
	public void insert(Edge item)
	{
		int j;
		
		for(j = 0; j < size; j++)
			if(item.distance >= queArray[j].distance)
				break;
		
			// move items up
		for(int k = size; k >= j; k--)
			queArray[k + 1] = queArray[k];
		
		queArray[j] = item;
		size++;
	}
	
		// remove minimum item
	public Edge removeMin()
	{
		return queArray[--size];
	}
	
		// remove item at n
	public void removeN(int n)
	{
		//System.out.println("Removing edge " + 
				//queArray[n].srcVert + "-" + queArray[n].destVert + ", distance " + queArray[n].distance);
		for(int j = n; j < size - 1; j++)
			queArray[j] = queArray[j + 1];
		size--;
	}
	
		// peek at minimum item
	public Edge peekMin()
	{
		return queArray[size - 1];
	}
	
		// return number of items
	public int size()
	{
		return size;
	}
	
		// true if queue is empty
	public boolean isEmpty()
	{
		return size == 0;
	}
	
		// peek at item n
	public Edge peekN(int n)
	{
		return queArray[n];
	}
	
		// find item with specified destVert value
	public int find(int findDex)
	{
		for(int j = 0; j < size; j++)
			if(queArray[j].destVert == findDex)
				return j;
		return -1;
	}
}//end class PriorityQ