//*******************************************************************
// Author: Neil Kasanda
//
// Program: implements an vertex of a weighted graph
// Listing 14.1
//*******************************************************************

public class Vertex
{
		// instance variables
	public char label;
	public boolean isInTree;
	
		// constructor
	public Vertex(char lab)
	{
		label = lab;
		isInTree = false;
	}
}// end class Vertex